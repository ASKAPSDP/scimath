/// @file
/// 
/// @brief an adapter for a generic estimator to use normal equation merge logic
/// @details This is a generic adapter to reuse normal equation merge logic with an arbitrary  
/// class called "Estimator" (because the typically envisaged use case for this class is to 
/// estimate some stats or secondary product on distributed data with merge operation is requried).
/// Note, conceptually we should have a design where tree-reduction is implemented for a
/// class which is a derivative of some "mergeable" type and can be cast to a particular interface
/// (and so normal equations should be possible to cast to this type as well), rather than the other
/// way around with an unrelated type adapted to work via normal equations. 
///
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_UTILS_ESTIMATOR_ADAPTER_H
#define ASKAP_SCIMATH_UTILS_ESTIMATOR_ADAPTER_H

// own includes
#include <askap/scimath/fitting/INormalEquations.h>
#include <askap/askap/AskapError.h>

// boost includes
#include <boost/shared_ptr.hpp>

// casacore includes
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Vector.h>

// other 3rd party
#include <Blob/BlobIStream.h>
#include <Blob/BlobOStream.h>

// std includes
#include <string>
#include <vector>
#include <type_traits>

namespace askap {

namespace scimath {

/// @brief default monitor type for estimator adapter
/// @details This class does nothing and expected to be optimised out
/// by the compiler. It is used as a default "monitor" type in the 
/// EstimatorAdapter template. Similar templates can be created and passed
/// to the adapter at declaration to alter behaviour of the merge operation
/// (namely to monitor it using knowledge of the actual estimator type).
/// In principle, this can even be done with a specialization of this default type.
template<typename Estimator>
struct EstimatorAdapterMonitor {
   inline static void aboutToMerge(const Estimator& /*thisEstimator*/, const Estimator& /*otherEstimator*/) {};
}; // struct EstimatorAdapterMonitor

/// @brief an adapter for a generic estimator to use normal equation merge logic
/// @details This is a generic adapter to reuse normal equation merge logic with an arbitrary  
/// class called "Estimator" (because the typically envisaged use case for this class is to 
/// estimate some stats or secondary product on distributed data with merge operation is requried).
/// Estimator type can be anything which is copy-constructable, has reset() method restoring the
/// object to a pristine state same as after default constructor (which may or may not be defined) and
/// also serializable (doesn't have to be derived from ISerializable, just needs writeToBlob and
/// readFromBlob methods to be defined).
/// Monitor type is an object function with aboutToMerge static method which is called inside merge with const references
/// to both Estimators being merged. By default, this monitor does nothing and will probably be optimised out by the compiler
template<typename Estimator, typename Monitor = EstimatorAdapterMonitor<Estimator> >
struct EstimatorAdapter : public scimath::INormalEquations {

   /// @brief constructor
   /// @details
   /// @param[in] estimator Estimator instance to wrap around (reference semantics)
   explicit EstimatorAdapter(const boost::shared_ptr<Estimator> &estimator) :
            itsEstimator(estimator) {ASKAPDEBUGASSERT(itsEstimator);}

   /// @brief Clone this into a shared pointer
   /// @details "Virtual constructor" - creates a copy of this object. Derived
   /// classes must override this method to instantiate an object of a proper
   /// type.
   virtual INormalEquations::ShPtr clone() const {
       ASKAPCHECK(itsEstimator, "Attempted to clone an empty EstimatorAdapter object");
       const boost::shared_ptr<Estimator> newEstimator = cloneEstimator();
       typedef EstimatorAdapter<Estimator, Monitor> ThisClassType;
       boost::shared_ptr<ThisClassType> result(new ThisClassType(newEstimator));
       return result;
   }

   /// @brief reset the normal equation object
   /// @details After a call to this method the object has the same pristine
   /// state as immediately after creation with the default constructor
   virtual void reset()
      { itsEstimator->reset(); }

   /// @brief Merge these normal equations with another
   /// @details Combining two normal equations depends on the actual class type
   /// (different work is required for a full matrix and for an approximation).
   /// This method must be overriden in the derived classes for correct
   /// implementation.
   /// This means that we just add
   /// @param[in] src an object to get the normal equations from
   virtual void merge(const INormalEquations& src)  {
       try {
           const EstimatorAdapter& ea = dynamic_cast<const EstimatorAdapter&>(src);
           Monitor::aboutToMerge(*itsEstimator, *ea.itsEstimator);
           itsEstimator->merge(*(ea.itsEstimator));
       }
       catch (const std::bad_cast &bc) {
           ASKAPTHROW(AskapError, "Unsupported type of normal equations used with the estimator adapter: "<<bc.what());
       }
   }

   

   /// @brief write the object to a blob stream
   /// @param[in] os the output stream
   virtual void writeToBlob(LOFAR::BlobOStream& os) const { 
       os.putStart("EstimatorAdapter", theirPayloadVersion); 
       itsEstimator->writeToBlob(os); 
       os.putEnd(); 
   }

   /// @brief read the object from a blob stream
   /// @param[in] is the input stream
   /// @note Not sure whether the parameter should be made const or not
   virtual void readFromBlob(LOFAR::BlobIStream& is) {
       const int version = is.getStart("EstimatorAdapter");
       ASKAPCHECK(version == theirPayloadVersion, "Payload version mismatch reading from blob");
       itsEstimator->readFromBlob(is); 
       is.getEnd();
   }


   /// @brief obtain shared pointer
   /// @return shared pointer to the estimator
   inline boost::shared_ptr<Estimator> get() const { return itsEstimator;}


   /// @brief stubbed method for this class
   /// @return nothing, throws an exception
   virtual const casacore::Matrix<double>& normalMatrix(const std::string &,
                        const std::string &) const
      { ASKAPTHROW(AskapError, "Method is not supported - this is an adapter"); }

   /// @brief stubbed method for this class
   /// @return nothing, throws an exception
   virtual const casacore::Vector<double>& dataVector(const std::string &) const
      { ASKAPTHROW(AskapError, "Method is not supported - this is an adapter"); }

   /// @brief stubbed method for this class
   /// @return nothing, throws an exception
   virtual std::vector<std::string> unknowns() const
      { ASKAPTHROW(AskapError, "Method is not supported - this is an adapter"); }

private:
   /// @brief clone estimator
   /// @details This helper method clones estimator via the copy constructor. The reason why
   /// this operation is abstracted out into a private method is to enable both copy-constructible and
   /// non-copy-constructible types.
   /// @return shared pointer to the new instance
   template<typename E = Estimator>
   typename std::enable_if<std::is_copy_constructible<E>::value, typename boost::shared_ptr<E> >::type
   cloneEstimator() const
   { 
       ASKAPDEBUGASSERT(itsEstimator);
       return boost::shared_ptr<E>(new E(*itsEstimator));
   }

   /// @brief clone estimator
   /// @details This helper method clones estimator via the copy constructor. The reason why
   /// this operation is abstracted out into a private method is to enable both copy-constructible and
   /// non-copy-constructible types.
   /// @return shared pointer to the new instance
   template<typename E = Estimator>
   typename std::enable_if<!std::is_copy_constructible<E>::value && std::is_default_constructible<E>::value, typename boost::shared_ptr<E> >::type
   cloneEstimator() const
   { 
       ASKAPDEBUGASSERT(itsEstimator);
       boost::shared_ptr<E> result(new E());
       result->merge(*itsEstimator);
       return result;
   }

   /// @brief estimator to work with, reference semantics
   boost::shared_ptr<Estimator> itsEstimator;

   /// @brief payload version for the serialisation
   static constexpr int theirPayloadVersion = 1;
};


} // namespace scimath

} // namespace askap

#endif // #ifndef ASKAP_SCIMATH_UTILS_ESTIMATOR_ADAPTER_H


