/// @file
/// 
/// @brief helper functions to do math of casa arrays in an optimised way
/// @details This file provides some optimised operations for casa arrays.        
///
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_UTILITY_OPTIMIZED_ARRAY_MATH_UTILS_TCC
#define ASKAP_UTILITY_OPTIMIZED_ARRAY_MATH_UTILS_TCC

// boost includes
#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>

// casacore includes
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/BasicSL/Complex.h>

// own includes
#include <askap/askap/AskapError.h>
#include <askap/scimath/utils/OptimizedArrayMathUtils.h>

namespace askap {

namespace utility {

/// @brief multiply matrix in situ by conjugated other matrix element by element
/// @details This is essentially what the A*=conj(B) does, but avoids
/// creating a temporary object for conj(B) reducing peak memory requirements.
/// In addition, some optimisations are done assuming contiguous memory arrays,
/// which is our most common use case in yandasoft.
/// @note We could've implemented it for the array in general, but matrix-specific
/// implementation seems to match our use case. Also note, that this is not a matrix multiplication
/// as per linear algebra, but rather element-by-element multiplication
/// @param[inout] in a matrix to change (in situ), i.e. A in A*=conj(B)
/// @param[in] other a matrix with elements to conjugate and multiply by (i.e B in A*=conj(B))
/// @ingroup utils
template<typename FT>
void multiplyByConjugate(casacore::Matrix<FT> &in, const casacore::Matrix<FT> &other)
{
   ASKAPASSERT(in.shape() == other.shape());
   ASKAPCHECK(in.contiguousStorage() && other.contiguousStorage(), "Logic error - utility::multiplyByConjugate can only work with contigous arrays!");
   const size_t size = in.nelements();
   // we don't take ownership of the data here, can work with raw pointer which is essentially the iterator 
   // (we can probably use cbegin/cend here, but working with raw pointer allows to make omp parallelism more safe)
   FT *inArr = in.data();
   const FT *otherArr = other.data();
   #pragma omp parallel for firstprivate(inArr, otherArr, size) schedule(static)
   for (size_t elem = 0; elem < size; ++elem) {
        // although it seems like a bit of the overhead to recalculate each element, OMP seems to require it
        // and we want OMP here because calling this specialised method only makes sense for large grids
        inArr[elem] *= conj(otherArr[elem]);
   }
}

/// @brief in place element by element array conjugation
/// @details This method conjugates every element of a complex array in an efficient manner.
/// @param[inout] in array to work with
/// @note This method relies on contiguous storage (and checks this) and also on the fact that complex number
/// is stored as two floats or doubles, with first being the real part and the second being the imaginary one. 
/// It assumes std::complex or equivalent (e.g. casacore types) and won't compile for real arrays.
template<typename FT>
void conjugateComplexArray(casacore::Array<FT> &in)
{
   // the following assertsensure code won't compile if an Array of a non-compliant type is passed
   BOOST_STATIC_ASSERT_MSG(boost::is_class<FT>::value, 
                           "utility::conjugateComplexArray can only be used with arrays of std::complex like types");
   BOOST_STATIC_ASSERT_MSG(sizeof(FT) == 2*sizeof(typename FT::value_type), 
                           "utility::conjugateComplexArray can only be used with arrays of std::complex like types");
   ASKAPCHECK(in.contiguousStorage(), "Logic error - utility::conjugateArray can only work with contigous arrays!");
   typename FT::value_type *inArr = reinterpret_cast<typename FT::value_type*>(in.data());
   const size_t size = 2*in.nelements();
   #pragma omp parallel for firstprivate(inArr,size) schedule(static)
   for (size_t elem = 1; elem < size; elem += 2) {
       inArr[elem] *= -1;
   }
}

/// @brief essentially an in place element by element multiplication of several arrays
/// @details This helper method implements the following operation in an optimised way
/// result = conj(A) * B * C / normFactor
/// where result and C are both casacore Arrays with identical shapes (degenerate dimensions are allowed) and
/// B and C are instances of casacore Matrix with the same shape as the arrays for the first two dimensions.
/// We could've had all parameters as arrays or matrices but doing it this way minimises the need for type conversion or
/// handling of degenerate dimensions by matching what we do in our current code.  
/// @param[out] result resulting array to write, must be initialised to the correct shape, old values are not used
/// @param[in] A matrix A in the above formula
/// @param[in] B matrix B in the above formula
/// @param[in] C array C in the above formula
/// @param[in] normFactor a scalar to divide by, see the above formula, the type should match the complex type used
/// @note All arrays and matrices should have contiguous storage, otherwise an exception is thrown
template<typename FT>
void calculateNormalisedProduct(casacore::Array<FT> &result, const casacore::Matrix<FT> &A, const casacore::Matrix<FT> &B, const casacore::Array<FT> &C, 
                                typename FT::value_type normFactor)
{
   ASKAPDEBUGASSERT(normFactor > 0.);
   ASKAPCHECK(result.contiguousStorage() && A.contiguousStorage() && B.contiguousStorage() && C.contiguousStorage(), 
              "Logic error - utility::calculateNormalisedProduct can only work with contigous arrays!");
   ASKAPCHECK(result.shape() == C.shape(), "The shape of the 'result' and 'C' array is different");
   ASKAPCHECK(A.shape() == B.shape(), "The shape of the 'A' and 'B' matrices is different");
   ASKAPCHECK(result.shape().nonDegenerate(2) == A.shape(), "Non-degenerate dimensions of the 'result' array should match that of the matrices");

   const size_t size = result.nelements();
   ASKAPDEBUGASSERT(A.nelements() == size);

   // we don't take ownership of the data here, can work with raw pointers which essentially act as iterators
   // (we can probably use cbegin/cend here, but working with raw pointer allows to make omp parallelism more safe)
   FT *resArray = result.data();
   const FT *aMatrix = A.data();
   const FT *bMatrix = B.data();
   const FT *cArray = C.data();

   #pragma omp parallel for firstprivate(resArray, aMatrix, bMatrix, cArray, size) schedule(static)
   for (size_t elem = 0; elem < size; ++elem) {
        // although it seems like a bit of the overhead to recalculate each element offset in a loop (=access it with operator[]), 
        // OMP seems to require it and we want OMP here because calling this specialised method only makes sense for large grids
        resArray[elem] = conj(aMatrix[elem]) * bMatrix[elem] * cArray[elem] / normFactor;
   }
}

/// @brief helper method to calculate the real part of the mean of a complex Array
/// @details This method is equivalent to sum(real(in)) / in.nelements(). Only contiguous arrays are supported.
/// Only arrays of std::complex like types are supported, it is a requirement that the complex type is equivalent to two 
/// adjacent floating point values with the first being the real part (this is the case for both std and casacore types).
/// @param[in] in array to work with
/// @return mean of the real part of the array
template<typename FT>
typename FT::value_type realPartMean(const casacore::Array<FT> &in)
{
   // the following assertsensure code won't compile if an Array of a non-compliant type is passed
   BOOST_STATIC_ASSERT_MSG(sizeof(FT) == 2*sizeof(typename FT::value_type), 
                           "utility::realPartMean can only be used with arrays of std::complex like types");
   ASKAPCHECK(in.contiguousStorage(), "Logic error - utility::conjugateArray can only work with contigous arrays!");
   const typename FT::value_type *arr = reinterpret_cast<const typename FT::value_type*>(in.data());
   const size_t size = 2*in.nelements();
   ASKAPCHECK(size > 0u, "An empty array passed to utility::realPartMean"); 
   typename FT::value_type sum = 0.;
   #pragma omp parallel for firstprivate(arr,size) reduction(+: sum) schedule(static)
   for (size_t elem = 0; elem < size; elem += 2) {
       sum += arr[elem]; 
   }
   // I expect that the compiler would easily optimise * 2 / size, so it would be (marginally) better than the in.nelements() function call
   return sum * 2 / size;
}

/// @brief add a Gaussian to the buffer provided
/// @details This method simulates 2D Gaussian with parameters provided and adds it to the 
/// buffer (which can be both complex and real)
/// @param[in] buffer the buffer to work with
/// @param[in] peak amplitude of the peak
/// @param[in] row_off offset in the first coordinate in pixels
/// @param[in] col_off offset in the second coordinate in pixels
/// @param[in] row_width width along the first coordinate in pixels
/// @param[in] col_width width along the second coordinate in pixels
/// @note this method is heavily used/tested in FFT2DTest
template<typename FT, typename T>
void addGaussian(casacore::Matrix<FT> &buffer, T peak, T row_off, T col_off, T row_width, T col_width)
{
  ASKAPASSERT(buffer.contiguousStorage());
  FT *data = buffer.data();
  const int nRow = static_cast<int>(buffer.nrow());
  const int nColumn = static_cast<int>(buffer.ncolumn());
  #pragma omp parallel for firstprivate(data, nRow, nColumn, peak, row_off, col_off, row_width, col_width) schedule(static)
  for (int col = 0; col < nColumn; ++col) {
       const T part = square((col - static_cast<T>(nColumn) / 2 - static_cast<T>(col_off)) / col_width);
       const int offset = col * nRow;
       for (int row = 0; row < nRow; ++row) {
            const T rad2 =  part + square((row - static_cast<T>(nRow) / 2 - static_cast<T>(row_off)) / row_width);
            data[offset+row] += peak * exp(-4.*log(2.) * rad2);
       }
  }
}


} // namespace utility

} // namespace askap


#endif // #ifndef ASKAP_UTILITY_OPTIMIZED_ARRAY_MATH_UTILS_TCC

