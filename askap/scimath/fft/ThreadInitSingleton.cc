/// @file
///
/// This singleton class controls initialisation of threading in FFTW (which is a global thing due to
/// the FFTW interface, and hence the use of the singleton pattern). It is very tempting to have a templated
/// implementation effectively to have a separate "singleton" for each precision option available (although this aspect
/// is poorly documented, it looks like we have to initialise for each type before using the appropriate methods) to
/// ensure we only run the appropriate init methods if necessary. However, this is likely to create a problem because
/// our code is distributed across many repos (and, therefore, shared libraries) and templates may be compiled separately
/// in different translation units which could lead to an undefined behaviour. So, the solution is more or less a
/// classical singleton, common for all data types. This singleton manages thread safety and guarantees that there is only 
/// one instance of the object as usual. However, instead of the templated class, there is a templated initialisation method
/// of the singleton which instantiates the appropriate object behind the scene and this object manages the lifecycle of the
/// FFTW threading data through the usual construction-destruction idiom. Individual guard objects (one for each supported
/// FFTW type) are all inherited from a common base guard object to store them easily in the common container 
/// (a map in our case). The check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably more readable code too. One could also use polymorphism for the
/// same purpose, but it is unlikely to be better in any way.
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated init<FT>() method for the type of interest (note, typing is done by the Fourier type, i.e. a complex of some sort)
/// before the first use of FFTW methods with multithreading:
///
/// \code{.cpp}
///    ThreadInitSingleton::getInstance().init<casacore::DComplex>();
/// \endcode
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

// boost include
#include <boost/thread/mutex.hpp>

// own include
#include <askap/scimath/fft/ThreadInitSingleton.h>

namespace askap {

namespace scimath {

/// @brief get instance method of the singleton
/// @details As per singleton design pattern with lazy initialisation, an instance is created on the first use
/// @return a non-const reference to the singleton object
ThreadInitSingleton& ThreadInitSingleton::getInstance()
{
   boost::mutex::scoped_lock lock(theirMutex);
   if (!theirInstance) {
       // need to initialise the singleton - this is the first use
       theirInstance.reset(new ThreadInitSingleton);
   }
   return *theirInstance;
}

// definition of static data members, doing it in a cc file guarantees to have just one copy in the scimath shared library

/// @brief the key singleton feature - static pointer to the instance
boost::shared_ptr<ThreadInitSingleton>  ThreadInitSingleton::theirInstance;

/// @brief mutex to protect data members
/// @details Use one common mutex for simplicity, although, in principle, separate mutex objects
/// could be used to protect singleton creation and the actual FFTW threading initialisation
/// (in particular, that second mutex could be per type of FFT; but performance implications of
/// having just one mutex should be small for our code as we don't mix FFTs with different precision
/// at the same stage of processing and there are usually other barriers in between)
boost::mutex ThreadInitSingleton::theirMutex;

} // namespace scimath

} // namespace askap
