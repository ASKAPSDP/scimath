/// @file
///
/// This is a helper template enabling type translation from casacore types to types used by FFTW.
/// It makes the code more generic and less hacky when we interface to low-level C library like FFTW.
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_FFT_WRAPPER_TYPE_TRAITS_TCC
#define ASKAP_SCIMATH_FFT_FFT_WRAPPER_TYPE_TRAITS_TCC

// FFTW
#include <fftw3.h>

// own includes
#include <askap/askap/AskapError.h>


namespace askap {

namespace scimath {

// specialisation for DComplex

/// @brief delete the plan
inline void FFTWrapperTypeTraits<casacore::DComplex>::PlanDeleter::operator()(plan_type plan) const
{
  boost::mutex::scoped_lock lock(itsMutex);
  if (plan != NULL) {
      fftw_destroy_plan(plan);
  }
}

/// @brief constructor - allocates resource
inline FFTWrapperTypeTraits<casacore::DComplex>::ThreadInitGuard::ThreadInitGuard() {
   #ifdef HAVE_FFTW_OPENMP
   const int status = fftw_init_threads();
   ASKAPCHECK(status != 0, "Error initialising threads for the double precision FFTW");
   #endif
}

/// @brief destructor- deallocates resource
inline FFTWrapperTypeTraits<casacore::DComplex>::ThreadInitGuard::~ThreadInitGuard() {
   #ifdef HAVE_FFTW_OPENMP
   fftw_cleanup_threads();
   #endif
}

// specialisation for Complex

/// @brief destroys the plan
inline void FFTWrapperTypeTraits<casacore::Complex>::PlanDeleter::operator()(plan_type plan) const {
   boost::mutex::scoped_lock lock(itsMutex);
   if (plan != NULL) {
       fftwf_destroy_plan(plan);
   }
}

/// @brief constructor - allocates resources
inline FFTWrapperTypeTraits<casacore::Complex>::ThreadInitGuard::ThreadInitGuard() {
   #ifdef HAVE_FFTW_OPENMP
   const int status = fftwf_init_threads();
   ASKAPCHECK(status != 0, "Error initialising threads for the single precision FFTW");
   #endif
}

/// @brief destructor- deallocates resource
inline FFTWrapperTypeTraits<casacore::Complex>::ThreadInitGuard::~ThreadInitGuard() {
   #ifdef HAVE_FFTW_OPENMP
   fftwf_cleanup_threads();
   #endif
}

} // namespace scimath

} // namespace askap

#endif // #ifndef ASKAP_SCIMATH_FFT_FFT_WRAPPER_TYPE_TRAITS_TCC
