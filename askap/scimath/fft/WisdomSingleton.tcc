/// @file
///
/// This singleton class controls management of FFTW wisdom files (which is a global thing due to
/// the FFTW interface, and hence the use of the singleton pattern). The idea is similar ThreadInitSingleton class, so
/// it was rather tempting just to add the appropriate functionality / methods there. But I (MV) think it is cleaner to 
/// have wisdom files managed by a separate class and avoid fat interfaces / blurring responsibilities between different
/// classes. We decided against clean up of the wisdom information. It is small, cumulative and doesn't need an explicit
/// free up according to the documentation. So, no guard classes similar to those used for threading initialisation are
/// required. However, we reuse the integer tag from the guard classes here to store information per precision option,
/// in a similar fashion to ThreadInitSingleton class. Another difference is the lack of thread synchronisation. 
/// We expect to use this class alongside the plan creation which is already protected by the mutex. So no additional
/// synchronisation is necessary, but care must be taken if this singleton is used directly (rather than through the 
/// FFT wrapper). It looks like FFTW wisdom file is specific to the number of threads used in the parallel case. The main
/// role of this class is to manage this complexity and the two precision options. It is expected that wisdom files 
/// have the name "fftw-[double|float]-Xthread(s).wisdom" where X is the number of OpenMP threads without leading zeros.
/// The wisdom files are expected to be located in the directory pointed to by FFTW_WISDOM environment variable. If the
/// variable is not defined, loading wisdom files will be inhibited and FFTs will resort to the usual ESTIMATE method.
/// Otherwise, the loading of wisdom file will be attempted at the beginning and every time the number of available threads
/// changes, separately for each available precision option. If the corresponding file is missing, FFTs will resport to the
/// ESTIMATE method and no further attempts to read this file will be made. If reading the file fails, an exception will be 
/// thrown.
///
/// As for ThreadInitSingleton, the check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably results in a more readable code too. 
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated loadWisdomFileIfNecessary<FT>(numThreads) method for the type of interest (note, typing is done by 
/// the Fourier type, i.e. a complex of some sort) just before plan creation. The parameter passed to the method is the number of
/// available threads.
///
/// \code{.cpp}
///    WisdomSingleton::getInstance().loadWisdomFileIfNecessary<casacore::DComplex>(numThreads);
/// \endcode
///
/// @note The fact that we have to create two separate singleton classes most likely suggests that we a better design could be just
/// one singleton corresponding to the whole FFTW library managing all its global data and precision options. It could have methods
/// returning handler objects for threading or wisdom management as appropriate. We may change the design if there is a need to 
/// integrate more operations like this.
///
///
/// @copyright (c) 2023 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_WISDOM_SINGLETON_TCC
#define ASKAP_SCIMATH_FFT_WISDOM_SINGLETON_TCC

// own include
#include <askap/scimath/fft/FFTWrapperTypeTraits.h>
#include <askap/askap/AskapUtil.h>

// casa includes
#include <casacore/casa/OS/Path.h>

// boost includes
#include <boost/static_assert.hpp>

namespace askap {

namespace scimath {

/// @brief templated method to load wisdom file for the given number of threads
/// @details The template parameter is the type of Fourier space point to define precision option.
/// @param[in] numThreads the number of available OpenMP threads (to load the appropriate file)
/// @note We could've used the appropriate real space type, but it is better this way for
/// consistency with the rest of the wrapper code. 
template<typename FT>
void WisdomSingleton::loadWisdomFileIfNecessary(int numThreads)
{
   typedef typename FFTWrapperTypeTraits<FT>::ThreadInitGuard GuardType;
   const int tag = GuardType::tag; 
   const std::map<int, int>::const_iterator ci = itsNumThreadsForWisdom.find(tag);
   if (ci != itsNumThreadsForWisdom.end()) {
      // initialisation has already been done for the given type. Check that the number of
      // threads is the same. 
      if (numThreads == ci->second) {
          // number of threads matches, so the suitable wisdom file has already been loaded
          // (or loading has been bypassed because FFTW_WISDOM is not defined)
          return;
      }
   }
   // no initialisation has been done so far for the given type or the number of threads
   itsNumThreadsForWisdom[tag] = numThreads;
   if (wisdomConfigured()) {
       // loading wisdom files has been configured, do it if the required file exists
       const std::string fileName = wisdomFileName<FT>(numThreads);
       if (exists(fileName)) {
           // attempt to load it
           logMessage("Attempt to load wisdom file "+fileName);
           loadWisdomFile<FT>(fileName);
       } else {
           // lack of wisdom file is not an error, just give a log message
           logMessage("Wisdom file "+fileName+" not found, proceeding without it");
       }
   }
}

/// @brief obtain wisdom file name corresponding to the given number of threads
/// @details The wisdom files are expected to have the name "fftw-[double|float]-Xthread(s).wisdom" where X 
/// is the number of OpenMP threads without leading zeros and be located in the directory pointed by FFTW_WISDOM
/// environment variable. This method returns the full name to the file translating the precision option and
/// the given number of threads. It throws an exception if the support of wisdom files has not been configured.
/// Use wisdomConfigured method to check if this is the case before calling this one.
/// @param[in] numThreads the number of available OpenMP threads (to be embedded in the file name)
/// @return full path to the wisdom file name (it may or may not exist)
/// @note This method is templated to be able to embed floating point precision in the file name 
/// (e.g. double or float as defined in the traits class). The template parameter is the type of the point in the
/// Fourier space (i.e. some kind of complex), as in the other parts of this wrapper.
template<typename FT>
std::string WisdomSingleton::wisdomFileName(int numThreads) const
{
   ASKAPCHECK(wisdomConfigured(), "No FFTW_WISDOM is defined - wisdomFileName shouldn't be called");
   ASKAPCHECK(numThreads > 0, "Number of threads is supposed to be positive, you have "<<numThreads);
   casacore::Path path(wisdomDirectory());
   const std::string trailingS = numThreads > 1 ? "s" : "";
   path.append("fftw-" + FFTWrapperTypeTraits<FT>::precisionString() + "-" + utility::toString(numThreads)+"thread"+trailingS+".wisdom");
   return path.absoluteName();
}

/// @brief actual low level method to load wisdom file
/// @details It just calls the appropriate FFTW method based on the template argument and check that
/// it doesn't error. We verify that the wisdom file exists before calling this method, so any error is a genuine error
/// and will generate an exception. 
/// @param[in] fileName name of the wisdom file to load
/// @note this is a generic catch version which won't compile (deliberately), only types with specialisation are supposed to be used
template<typename FT>
void WisdomSingleton::loadWisdomFile(const std::string &fileName) 
{
   // this generic version shouldn't be built
   BOOST_STATIC_ASSERT(sizeof(FT) == 0);
}

/// @brief actual low level method to load wisdom file for single precision
/// @details It just calls the appropriate FFTW method based on the template argument and checks that
/// it doesn't error. We verify that the wisdom file exists before calling this method, so any error is a genuine error
/// and will generate an exception. 
/// @param[in] fileName name of the wisdom file to load
template<>
inline void WisdomSingleton::loadWisdomFile<casacore::Complex>(const std::string &fileName) 
{
   const int status = fftwf_import_wisdom_from_filename(fileName.c_str());
   ASKAPCHECK(status != 0, "Error loading single precision wisdom file "<<fileName<<" status = "<<status);     
}

/// @brief actual low level method to load wisdom file for single precision
/// @details It just calls the appropriate FFTW method based on the template argument and checks that
/// it doesn't error. We verify that the wisdom file exists before calling this method, so any error is a genuine error
/// and will generate an exception. 
/// @param[in] fileName name of the wisdom file to load
template<>
inline void WisdomSingleton::loadWisdomFile<casacore::DComplex>(const std::string &fileName) 
{
   const int status = fftw_import_wisdom_from_filename(fileName.c_str());
   ASKAPCHECK(status != 0, "Error loading double precision wisdom file "<<fileName<<" status = "<<status);     
}

/// @brief low level method to export a wisdom file
/// @details It is handy to keep all wisdom-related operations in this class. To be able to generate wisdom files we
/// need to write them eventually after the appropriate planning is done. We expect to use this method from the specialised
/// routine generating wisdom files. In normal operations (when we just read wisdom files), it is not called. As the case of
/// all methods of this class, there is no concurrency protention of any kind, so the user should worry about thread synchronisation
/// if this method is used in parallel environment.
/// @note This method just calles the appropriate FFTW routine for the given data type. It doesn't require FFTW_WISDOM
/// variable to be defined, but we expect to use it with a name generated by wisdomFileName which does require FFTW_WISDOM to be
/// defined. Any error returned by the FFTW routine is a genuine one and will generate an exception.
/// @param[in] fileName name of the wisdom file to write
template<typename FT>
void WisdomSingleton::writeWisdomFile(const std::string &fileName)
{
   // this generic version shouldn't be built
   BOOST_STATIC_ASSERT(sizeof(FT) == 0);
}

/// @brief low level method to export a wisdom file
/// @details It is handy to keep all wisdom-related operations in this class. To be able to generate wisdom files we
/// need to write them eventually after the appropriate planning is done. We expect to use this method from the specialised
/// routine generating wisdom files. In normal operations (when we just read wisdom files), it is not called. As the case of
/// all methods of this class, there is no concurrency protention of any kind, so the user should worry about thread synchronisation
/// if this method is used in parallel environment.
/// @note This method just calles the appropriate FFTW routine for the given data type. It doesn't require FFTW_WISDOM
/// variable to be defined, but we expect to use it with a name generated by wisdomFileName which does require FFTW_WISDOM to be
/// defined. Any error returned by the FFTW routine is a genuine one and will generate an exception.
/// @param[in] fileName name of the wisdom file to write
template<>
inline void WisdomSingleton::writeWisdomFile<casacore::DComplex>(const std::string &fileName)
{
   const int status = fftw_export_wisdom_to_filename(fileName.c_str());
   ASKAPCHECK(status != 0, "Error writing double precision wisdom file "<<fileName<<" status = "<<status);     
}

/// @brief low level method to export a wisdom file
/// @details It is handy to keep all wisdom-related operations in this class. To be able to generate wisdom files we
/// need to write them eventually after the appropriate planning is done. We expect to use this method from the specialised
/// routine generating wisdom files. In normal operations (when we just read wisdom files), it is not called. As the case of
/// all methods of this class, there is no concurrency protention of any kind, so the user should worry about thread synchronisation
/// if this method is used in parallel environment.
/// @note This method just calles the appropriate FFTW routine for the given data type. It doesn't require FFTW_WISDOM
/// variable to be defined, but we expect to use it with a name generated by wisdomFileName which does require FFTW_WISDOM to be
/// defined. Any error returned by the FFTW routine is a genuine one and will generate an exception.
/// @param[in] fileName name of the wisdom file to write
template<>
inline void WisdomSingleton::writeWisdomFile<casacore::Complex>(const std::string &fileName)
{
   const int status = fftwf_export_wisdom_to_filename(fileName.c_str());
   ASKAPCHECK(status != 0, "Error writing single precision wisdom file "<<fileName<<" status = "<<status);     
}

} // namespace scimath

} // namespace askap

#endif // #ifndef ASKAP_SCIMATH_FFT_WISDOM_SINGLETON_TCC
