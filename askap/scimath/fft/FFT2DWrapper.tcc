/// @file
///
/// FFT2D wrapper is an alternative wrapper of FFTW, specific to 2D transforms. It provides similar interface but
/// relies on low-level representation of casacore types. Also it is a bit more careful with locking related to shared
/// memory parallel processing and when the plan is rebuilt.
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_FFT_2D_WRAPPER_TCC
#define ASKAP_SCIMATH_FFT_FFT_2D_WRAPPER_TCC

// ASKAPsoft includes
#include "askap/askap/AskapError.h"
#include "askap/askap/AskapUtil.h"
#include "askap/scimath/fft/WisdomSingleton.h"
#include "askap/askap/AskapLogging.h"
#include "askap/scimath/fft/FFTUtils.h"

// FFTW
#include <fftw3.h>

// boost
#include <boost/static_assert.hpp>
#include <boost/shared_array.hpp>

// std
#include <algorithm>

// lofar (for OpenMP wrapper)
#include <Common/OpenMP.h>

// casacore
#include <casacore/casa/Arrays/ArrayIter.h>

namespace askap {

namespace scimath {

/// @brief the main entry point, i.e. the method which does 2D FFT
/// @param[in] data 2D array to transform
/// @param[in] forward true for a forward transform, false for the inverse
/// @note The method is deliberately made non-const to highlight the fact
/// that it may rebuild the plan which is cached. Make the whole instance of
/// the class mutable if you need to have it as a data member of another class and
/// use it from const methods.
template<typename FT>
void FFT2DWrapper<FT>::operator()(casacore::Matrix<FT> &data, bool forward)
{
   ASKAPASSERT(data.nelements()>0);
   ASKAPCHECK(data.contiguousStorage(), "This high-performance interface requires contiguous storage - logic error");
   const casacore::IPosition shape = data.shape();
   ASKAPDEBUGASSERT(shape.size() == 2);
   // helper shared pointer to the start of the array (just to avoid dealing with raw pointers here and make the code nicer)
   // no ownership transfer takes place here
   const typename boost::shared_ptr<typename FT::value_type>
        buffer(reinterpret_cast<typename FT::value_type *>(data.data()), utility::NullDeleter());
   const typename FFTWrapperTypeTraits<FT>::shared_plan_ptr plan = rebuildPlanIfNecessary(shape, buffer, forward);
   // there are many ways dealing with harmonic transposition here and this could be made user-configurable
   // for now, hard-code options which were shown to be the best in our particular case based on profiling exercise
   // and comment out other options for future reference, just in case it becomes useful later on
   //
   // it is possible to select whether harmonics are centered (and how they are reordered) at compile time via templates, but
   // do it more explicitly, at least for now - the difference would be insignificant anyway
   if (itsZeroAtCentre) {
       // instead of doing more expensive permutations we can use the trick of pre-modifying input array which is faster
       if (shape[0] % 2 == 0 && shape[1] % 2 == 0) {
           // however, doing it via sign permutation works only for even sizes (not necessarily powers of 2)
           flipSigns(shape, buffer);
       } else {
           // generic version (as this is just for completeness/investigation purposes rather than operational use,
           // the interface accepts the original matrix rather than buffer, but overheads are low anyway)
           //
           // option 1 - via phase gradient
           //applyPhaseGradient(data, forward, false);
           //
           // option 2 - rearrangement via data copy
           fftShift(data, false);
           // other implementations are possible and considered in the literature, e.g. std::rotate based implementation with matrix transposition
       }
   }
   // now do the FFT itself
   executePlan(plan, buffer);
   if (itsZeroAtCentre) {
       if (shape[0] % 2 == 0 && shape[1] % 2 == 0) {
           // shift via sign permutation works only for even sizes (not necessarily powers of 2)
           // note, another sign flip for the whole array might have been needed if we had an odd number of dimensions
           flipSigns(shape, buffer);
       } else {
           // generic version (as this is just for completeness/investigation purposes rather than operational use,
           // the interface accepts the original matrix rather than buffer, but overheads are low anyway)
           //
           // option 1 - via phase gradient
           //applyPhaseGradient(data, forward, true);
           //
           // option 2 - rearrangement via data copy
           fftShift(data, true);
           // other implementations are possible and considered in the literature, e.g. std::rotate based implementation with matrix transposition
       }
   }
   // normalise (FFTW output is unnormalised), we can make it optional if necessary or merge with sign flips (although this didn't seem to give
   // much difference for our image sizes)
   if (!forward) {
       data /= static_cast<typename FT::value_type>(data.shape().product());
   }
}

/// @brief convenience method to transform all hyperplanes of an array serially
/// @details This method does serial iteration of all hyperplanes (i.e. dimensions beyond the
/// first two) of a casacore Array by calling operator() in a loop. The iteration is done
/// serially and a 2D slice is taken meaning that this method is not thread safe
/// (unlike the main entry point, operator(), which is thread safe). This is not a fundamental
/// limitation and can probably be removed later on. For 2D arrays it is equivalent to
/// operator(), except the lack of thread safety.
/// @param[in] data ND array to transform (N>=2)
/// @param[in] forward true for a forward transform, false for reverse
/// @note I (MV) resisted to provide array interface early on because managing thread safety is
/// difficult this way. But perhaps this convenience method is not a step backwards as long as
/// we remember that this is only for serial scenarios and the main intention is to reduce code
/// duplication elsewhere (and remember that it is a bad idea to use it instead of operator() just
/// to avoid a type cast or changing the code to use the proper type). Each 2D hyperplane of the 
/// array should be contiguous in memory. It is also possible to 
/// write an OpenMP version parallelising by hyperplanes. However, it needs some experimentation as
/// the interaction with OpenMP in the FFTW may be non-trivial.
template<typename FT>
void FFT2DWrapper<FT>::transformAllHyperPlanes(casacore::Array<FT> &data, bool forward)
{
   ASKAPASSERT(data.shape().size() >= 2);
   for (casacore::ArrayIterator<FT> it(data, 2); !it.pastEnd(); it.next()) {
        casacore::Matrix<FT> slice(it.array());
        operator()(slice, forward);
   }
}

/// @brief generalisation of flipSigns approach, works with odd matrix sizes
/// @details This method implements a general procedure of shifting harmonics via applying a phase gradient in the other domain. It becomes flipSigns
/// if the matrix has even sizes on both axes.
/// @param[in] data matrix to work with (modified in situ)
/// @param[in] forward true for forward transform, false for the reverse
/// @param[in] afterFFT true for the gradient applied after FFT, false for the gradient applied before FFT (matters in the odd size case)
template<typename FT>
void FFT2DWrapper<FT>::applyPhaseGradient(casacore::Matrix<FT> &data, bool forward, bool afterFFT)
{
   const casacore::IPosition shape = data.shape();
   ASKAPDEBUGASSERT(shape.size() >= 2);
   ASKAPDEBUGASSERT(shape.product() > 0);

   // additional phase necessary to account for the shifted centre in the other domain
   // it disappears if the size is integral multiple of 4, or is even for both axes
   // we also only need it for phase rotation performed after the FFT
   double addPhase = 0.;
   if (afterFFT) {
       // the centre is always at int(N)/2 for both even and odd N
       const casacore::Int halfSizeRow = shape[0] / 2;
       const casacore::Int halfSizeCol = shape[1] / 2;
       // don't bother with the special case of size being integral multiple of 4 here, or the even vs odd N
       // (although it could lead to some marginal performance improvement)
       addPhase += (forward ? -1 : +1) * casacore::C::_2pi*halfSizeRow*halfSizeRow / shape[0];
       addPhase += (forward ? -1 : +1) * casacore::C::_2pi*halfSizeCol*halfSizeCol / shape[1];
   }

   // the formulas below imply the centre at int(N)/2, just avoid unnecessary operations (although compiler would probably optimise them out anyway)
   // also, we seem to need doubles here regardless of the FT::value_type, otherwise precision of the result is notably affected
   const double factorRow = casacore::C::pi*(shape[0] % 2 == 0 ? 1. : (forward ? 1 : -1) * static_cast<double>(shape[0]-1) / shape[0]);
   const double factorCol = casacore::C::pi*(shape[1] % 2 == 0 ? 1. : (forward ? 1 : -1) * static_cast<double>(shape[1]-1) / shape[1]);

   // these tables of multiplier factors can probably be cached as they only change when the shape changes
   // leave as is for now, because this code is not currently used and only left here to help our understanding in the future, if more work on this is required
   boost::shared_array<FT> rowMult(new FT[data.nrow()]);
   #pragma omp parallel for
   for (casacore::Int row = 0; row<static_cast<casacore::Int>(data.nrow()); ++row) {
        // again, need double phase here regardless of the FT type
        const double phase = row * factorRow;
        rowMult[row] = FT(cos(phase), sin(phase));
   }

   // row and column factors are the same if the grid is square, rely on reference semantics in this case
   boost::shared_array<FT> colMult = rowMult;
   if (data.ncolumn() != data.nrow()) {
       colMult.reset(new FT[data.ncolumn()]);
       #pragma omp parallel for
       for (casacore::Int col = 0; col < static_cast<casacore::Int>(data.ncolumn()); ++col) {
            // again, need double phase here regardless of the FT type
            const double phase = col * factorCol;
            colMult[col] = FT(cos(phase), sin(phase));
       }
   }
   const FT addPhaseMult(cos(addPhase), sin(addPhase));

   for (casacore::Int col = 0; col < static_cast<casacore::Int>(data.ncolumn()); ++col) {
        const FT colAndAddPhaseMult = colMult[col] * addPhaseMult;
        #pragma omp parallel for
        for (casacore::Int row = 0; row < static_cast<casacore::Int>(data.nrow()); ++row) {
             // the original version without caching of factors (found to be notably slower)
             // (note, the precision is notably better if phase is kept double in all cases, but it is notably slower)
             //const double phase = row * factorRow + col * factorCol + addPhase;
             //data(row,col) *= FT(cos(phase), sin(phase));
             data(row,col) *= colAndAddPhaseMult * rowMult[row];
        }
   }


   /*
   // amazingly, C-style code has pretty much the same performance on large images as the above C++ version
   // perhaps, dealing with complex as two real numbers would change this - some experiments may be worth while, if we
   // are going to use this method of dealing with harmonic swaps at all
   FT *ptr = data.data();
   const FT *end = ptr + shape.product();
   const FT *cmEnd = colMult.get() + shape[1];
   const FT *rmStart = rowMult.get();
   const FT *rmEnd = rmStart + shape[0];
   for (const FT *cmPtr = colMult.get(); cmPtr < cmEnd; ++cmPtr) {
        const FT colAndAddPhaseMult = (*cmPtr) * addPhaseMult;
        for (const FT *rmPtr = rmStart; rmPtr < rmEnd; ++rmPtr,++ptr) {
             (*ptr) *= colAndAddPhaseMult * (*rmPtr);
        }
   }
   */
}

/// @brief helper method analogous to std::rotate but for a strided sequence
/// @details We could've written a strided iterator. I (MV) tried to reuse one from
/// boost but the appropriate module seems to require OpenCL. We also could've only kept
/// implementation for It == FT*, but doing it this way doesn't seem to hurt performance
/// @param[in] first iterator to the first element of the sequence
/// @param[in] middle iterator to the new first element after rotation
/// @param[in] last end iterator
/// @param[in] stride stride to add to iterators to advance them.
/// @note We could've used std::advance but given it is only used with pointer types, it is fine to
/// have iterators advancing with += operator
template<typename FT>
template<typename It>
void FFT2DWrapper<FT>::stridedRotate(It first, It middle, It last, casacore::Int stride)
{
   It next = middle;
   while (first != next) {
          swap(*first,*next);
          first += stride;
          next += stride;
          if (next == last) {
              next = middle;
          } else {
            if (first == middle) {
                middle = next;
            }
          }
   }
}

/// @brief harmonic/data rearrangement via direct data move
/// @details This method serves the same purpose as applyPhaseGradient (or flipSigns) but does the operation via moving the data in the original domain
/// (instead of the phase gradient, or its degenerate version = the sign flips, in the other domain)
/// @param[in] data matrix to work with (modified in situ)
/// @param[in] afterFFT true if run after FFT in our case
template<typename FT>
void FFT2DWrapper<FT>::fftShift(casacore::Matrix<FT> &data, bool afterFFT)
{
   const casacore::IPosition shape = data.shape();
   ASKAPDEBUGASSERT(shape.size() == 2);
   ASKAPDEBUGASSERT(shape[0] > 0);
   ASKAPDEBUGASSERT(shape[1] > 0);

   const casacore::Int xshift = !afterFFT ? shape[0] / 2 : (shape[0] + 1) / 2;
   const casacore::Int yshift = !afterFFT ? shape[1] / 2 : (shape[1] + 1) / 2;

   // no transfer of ownership, using raw pointer is safe here
   // (although we could also wrap it into the shared pointer with NullDeleter)
   FT *storage = data.data();
   #pragma omp parallel for
   for (casacore::Int col = 0; col < shape[1]; ++col) {
        FT *start = storage + col * shape[0];
        FT *end = storage + (col + 1) * shape[0];
        std::rotate(start, start + xshift, end);
   }
   #pragma omp parallel for
   for (casacore::Int row = 0; row < shape[0]; ++row) {
        FT *start = storage + row;
        // a bit ugly, as "end" is way beyond the bounds of the array but matches strided behaviour
        FT *end = start + shape[0] * shape[1];
        FT *middle = start + yshift * shape[0];
        stridedRotate(start, middle, end, shape[0]);
   }
}

/// @brief helper method to alternate signs
/// @details FFTW doesn't put zero harmonic in the centre. Instead of using more expensive reordering after transform we can multiply
/// certain elements of the input array by -1 (i.e. flip the sign) in some pattern which depends on the shape of the array. This is more efficient.
/// @param[in] shape shape of the next array to transform
/// @param[in] data pointer to the data buffer (in-place transform is assumed)
/// @note we could've embedded scaling into this procedure too
template<typename FT>
void FFT2DWrapper<FT>::flipSigns(const casacore::IPosition &shape, const typename boost::shared_ptr<typename FT::value_type> &data)
{
   ASKAPDEBUGASSERT(shape.size() == 2);
   // quite low level C-like code below to simplify vectorisation. All it does is multiplies complex numbers (stored as real, imag)
   // by (-1)^{i+j} where i,j are row and column numbers
   typename FT::value_type *start = data.get();
   typename FT::value_type *end = start + shape.product()*2;

   // casacore arrays are column-major (i.e. fortran order)
   const int oneColumn = shape[0]*2;
   const int twoColumns = 2*oneColumn;
   for (typename FT::value_type *colptr = start + oneColumn; colptr < end; colptr += twoColumns) {
        #pragma omp parallel for
        for (typename FT::value_type *ptr = colptr; ptr < colptr + oneColumn; ptr+=4) {
             *ptr *= -1;
             *(ptr+1) *= -1;
        }
   }

   for (typename FT::value_type *colptr = start; colptr < end; colptr += twoColumns) {
        #pragma omp parallel for
        for (typename FT::value_type *ptr = colptr + 2; ptr < colptr + oneColumn; ptr+=4) {
             *ptr *= -1;
             *(ptr+1) *= -1;
        }
   }
}

/// @brief check the plan is suitable and rebuild it if necessary
/// @details All non-threadsafety is contained in this method and is protected by the mutex.
/// To simplify this, the interface accepts low-level FFTW types instead of extracting this info
/// from the casacore's array.
/// @param[in] shape shape of the next array to transform
/// @param[in] data pointer to the data buffer (in-place transform is assumed)
/// @param[in] forward true, if the forward transform is to be performed
/// @return shared pointer to the plan (same as stored in itsPlan, but it is handy to return it as it allows us to
/// encapsulate all mutex-protected access to caches inside this method
template<typename FT>
typename FFTWrapperTypeTraits<FT>::shared_plan_ptr
FFT2DWrapper<FT>::rebuildPlanIfNecessary(const casacore::IPosition &shape, const typename boost::shared_ptr<typename FT::value_type> &data, bool forward)
{
   ASKAPASSERT(!shape.empty());
   const int index = directionIndex(forward);
   ASKAPDEBUGASSERT(index < 2);

   // note, it's essential that oldPlan is declared before mutex lock. We use this buffer to store shared pointer on the old
   // plan, in the case when it needs to be replaced. Declaring oldPlan before mutex lock ensures that it is destructed _after_ the lock and
   // therefore calls the appropriate deleter using the same mutex while it is already in the unlocked state. Otherwise (or if we don't have
   // this buffer at all), a dead-lock occurs.
   typename FFTWrapperTypeTraits<FT>::shared_plan_ptr oldPlan;
   boost::mutex::scoped_lock lock(itsMutex);
   //

   const int newBufferAlignment = getAlignment(data);
   if (shape.isEqual(itsPlanShape[index])) {
       // shared pointer should not be empty if the shape matches
       ASKAPASSERT(itsPlan[index]);
       if (itsPlanAlignment[index] == newBufferAlignment) {
           // plan passes the checks - we can reuse it
           return itsPlan[index];
       }
   }

   // we have to rebuild the plan

   // without multi-threading compiled in the following call would just return 1, so safe to use it
   const int numThreads = getMaxThreads();
   // the following call is no operation if there is no multi-threading support (at all or just in FFTW)
   configurePlanningWithThreads(numThreads);

   WisdomSingleton::getInstance().loadWisdomFileIfNecessary<FT>(numThreads);

   itsPlanShape[index] = shape;
   itsPlanAlignment[index] = newBufferAlignment;
   // we don't need to worry about destructor and about destroying the plan, shared pointer with the
   // appropriate deleter (assigned in  makeSharedPlanPtr) will do it for us when an instance of this
   // class goes out of scope. To avoid a dead-lock when itsPlan[index] already has something, we store
   // that shared pointer in the oldPlan variable which is destroyed _after_ the mutex object which unlocks it
   oldPlan = itsPlan[index];
   itsPlan[index] = makePlan(shape, data, forward, FFTW_ESTIMATE);
   ASKAPCHECK(itsPlan[index], "Unable to create plan for shape = "<<shape<<", perhaps planning flags are too restrictive");
   // this will initiate shared pointer's copy constructor which is thread safe, access here is still protected
   // by the mutex, oldPlan will be destroyed after the lock is unlocked when this method concludes
   return itsPlan[index];
}

/// @brief execute plan
/// @details Wrapper around FFTW methods executing the plan
/// @param[in] plan plan to use
/// @param[in] data pointer to the data buffer
template<typename FT>
void FFT2DWrapper<FT>::executePlan(const typename FFTWrapperTypeTraits<FT>::shared_plan_ptr &plan,
                           const typename boost::shared_ptr<typename FT::value_type> &data)
{
   // this generic version shouldn't be built
   BOOST_STATIC_ASSERT(sizeof(FT) == 0);
}

// specialisations for the actual types we use
template<>
inline void FFT2DWrapper<casacore::Complex>::executePlan(const typename FFTWrapperTypeTraits<casacore::Complex>::shared_plan_ptr &plan,
                          const typename boost::shared_ptr<float> &data)
{
   // MV: there could be a better way of dealing with types here, but unfortunately fftwf_complex is float[2]
   // which is not the same as fftwf_complex* when it is passed in the shared pointer. Although we could've safely pass raw pointer
   // to this particular method it is nice to adhere to general principle of no raw pointers in the interface
   fftwf_complex* buf = reinterpret_cast<fftwf_complex*>(data.get());
   fftwf_execute_dft(plan.get(), buf, buf);
}

template<>
inline void FFT2DWrapper<casacore::DComplex>::executePlan(const typename FFTWrapperTypeTraits<casacore::DComplex>::shared_plan_ptr &plan,
                          const typename boost::shared_ptr<double> &data)
{
   // MV: there could be a better way of dealing with types here, but unfortunately fftw_complex is double[2]
   // which is not the same as fftw_complex* when it is passed in the shared pointer. Although we could've safely pass raw pointer
   // to this particular method it is nice to adhere to general principle of no raw pointers in the interface
   fftw_complex* buf = reinterpret_cast<fftw_complex*>(data.get());
   fftw_execute_dft(plan.get(), buf, buf);
}


/// @brief helper method to get alignment of the buffer
/// @details This is to ensure we always use high-performance algorithm underneath (although on modern machines it may be possible to short cut these
/// checks)
/// @param[in] data pointer to the data buffer
/// @return integer alignment info as returned by FFTW
template<typename FT>
int FFT2DWrapper<FT>::getAlignment(const typename boost::shared_ptr<typename FT::value_type> &data)
{
   // this generic version shouldn't be built
   BOOST_STATIC_ASSERT(sizeof(FT) == 0);
   // just to keep compiler happy
   return 0;
}

// specialisations for the actual types we use
template<>
inline int FFT2DWrapper<casacore::Complex>::getAlignment(const typename boost::shared_ptr<float> &data)
{
   return fftwf_alignment_of(data.get());
}

template<>
inline int FFT2DWrapper<casacore::DComplex>::getAlignment(const typename boost::shared_ptr<double> &data)
{
   return fftw_alignment_of(data.get());
}

/// @brief low-level method to enable planning with threads
/// @details This has to be called before making a plan or loading wisdom file if using 
/// OpenMP threads. The desired number of threads is the parameter. In normal operations, we pass
/// the number of threads returned by getMaxThreads. This method just calls the FFTW routine 
/// appropriate for the data type. If the library is compiled without OpenMP support (at all or
/// just without OpenMP-enabled FFT), this method does nothing (call to FFTW is not compiled as
/// the appropriate FFTW routine may not even exist).
/// @note In normal operations this method is called inside rebuildPlanIfNecessary and is protected by
/// mutex, so no additional synchronisation is necessary. We also use it in wisdom generation, but
/// this is not expected to be done in parallel at the moment.
/// @param[in] numThreads number of threads to use for the planning
template<typename FT>
void FFT2DWrapper<FT>::configurePlanningWithThreads(int numThreads)
{
   // this generic version shouldn't be built
   BOOST_STATIC_ASSERT(sizeof(FT) == 0);
}

/// @brief low-level method to enable planning with threads
/// @details Specialisation for DComplex
/// @param[in] numThreads number of threads to use for the planning
template<>
inline void FFT2DWrapper<casacore::DComplex>::configurePlanningWithThreads(int numThreads)
{
   #ifdef HAVE_FFTW_OPENMP
   fftw_plan_with_nthreads(numThreads);
   #endif
}

/// @brief low-level method to enable planning with threads
/// @details Specialisation for Complex
/// @param[in] numThreads number of threads to use for the planning
template<>
inline void FFT2DWrapper<casacore::Complex>::configurePlanningWithThreads(int numThreads)
{
   #ifdef HAVE_FFTW_OPENMP
   fftwf_plan_with_nthreads(numThreads);
   #endif
}

/// @brief helper method to create shared pointer to a plan
/// @details It helps not to forget PlanDeleter + constructs the right type and provides synchronisation using 
/// the same mutex as used in plan creation.
/// @param[in] plan plan returned by an FFTW routine 
/// @return shared pointer to the plan
template<typename FT>
typename FFTWrapperTypeTraits<FT>::shared_plan_ptr FFT2DWrapper<FT>::makeSharedPlanPtr(typename FFTWrapperTypeTraits<FT>::plan_type plan)
{
   typedef typename FFTWrapperTypeTraits<FT>::shared_plan_ptr ShPtr;
   typedef typename FFTWrapperTypeTraits<FT>::PlanDeleter Deleter;
   return ShPtr(plan, Deleter(itsMutex));
}

/// @brief helper method to make the plan
/// @details This method encapsulates interface differences for different types (FT), it is called from rebuildPlanIfNecessary
/// @param[in] shape shape of the next array to transform
/// @param[in] data pointer to the data buffer (in-place transform is assumed)
/// @param[in] forward true, if the forward transform is to be performed
/// @param[in] rigor planning-rigor flags which can be or'ed (e.g. FFTW_ESTIMATE). Note, only FFTW_ESTIMATE and FFTW_WISDOM_ONLY can be used
///            in normal operations via rebuildPlanIfNecessary as other planning methods overwrite the input array. We can use other flags 
///            when generating wisdom files.
/// @return shared pointer to the plan
template<typename FT>
typename FFTWrapperTypeTraits<FT>::shared_plan_ptr
FFT2DWrapper<FT>::makePlan(const casacore::IPosition &shape, const typename boost::shared_ptr<typename FT::value_type> &data, bool forward, unsigned int rigor)
{
   // this generic version shouldn't be built
   BOOST_STATIC_ASSERT(sizeof(FT) == 0);
   // just to keep compiler happy
   return typename FFTWrapperTypeTraits<FT>::shared_plan_ptr();
}

// specialisation for the actual types we use

template<>
inline typename FFTWrapperTypeTraits<casacore::Complex>::shared_plan_ptr
FFT2DWrapper<casacore::Complex>::makePlan(const casacore::IPosition &shape, const typename boost::shared_ptr<float> &data, bool forward, unsigned int rigor)
{
   ASKAPDEBUGASSERT(shape.size() == 2);

   // MV: there could be a better way of dealing with types here, but unfortunately fftwf_complex is float[2]
   // which is not the same as fftwf_complex* when it is passed in the shared pointer. Although we could've safely pass raw pointer
   // to this particular method it is nice to adhere to general principle of no raw pointers in the interface
   fftwf_complex* buf = reinterpret_cast<fftwf_complex*>(data.get());
   return makeSharedPlanPtr(fftwf_plan_dft_2d(shape[1], shape[0], buf, buf, forward ? FFTW_FORWARD : FFTW_BACKWARD, rigor));
}

template<>
inline typename FFTWrapperTypeTraits<casacore::DComplex>::shared_plan_ptr
FFT2DWrapper<casacore::DComplex>::makePlan(const casacore::IPosition &shape, const typename boost::shared_ptr<double> &data, bool forward, unsigned int rigor)
{
   ASKAPDEBUGASSERT(shape.size() == 2);

   // MV: there could be a better way of dealing with types here, but unfortunately fftw_complex is double[2]
   // which is not the same as fftw_complex* when it is passed in the shared pointer. Although we could've safely pass raw pointer
   // to this particular method it is nice to adhere to general principle of no raw pointers in the interface
   fftw_complex* buf = reinterpret_cast<fftw_complex*>(data.get());
   return makeSharedPlanPtr(fftw_plan_dft_2d(shape[1], shape[0], buf, buf, forward ? FFTW_FORWARD : FFTW_BACKWARD, rigor));
}

/// @brief get largest permitted number of OpenMP threads
/// @details This method returns the smallest of itsMaxThreads, if it is positive and the current value of
/// omp_get_max_threads() obtained via the LOFAR's wrapper (which returns 1 if no OpenMP is available).
/// If itsMaxThreads is negative (this is the default), the current value of omp_get_max_threads() is returned.
/// If the base-scimath library has been compiled without OpenMP support, getMaxThreads method returns 1.
/// @return maximum number of threads to use for FFT plan execution
template<typename FT>
int FFT2DWrapper<FT>::getMaxThreads() const
{
   #ifdef HAVE_FFTW_OPENMP
   // although LOFAR's implementation is absolutely correct that the number of threads is unsigned int,
   // it is worth to preserve "int" as the return type here because it is passed directly to an FFTW call
   // which takes int. Added static_cast for clarity, although we could've relied on implicit type cast.
   const int defaultMaxThreads = static_cast<int>(LOFAR::OpenMP::maxThreads());
   return itsMaxThreads > 0 ? std::min(itsMaxThreads, defaultMaxThreads) : defaultMaxThreads;
   #else
   return 1;
   #endif
}

/// @brief generate FFTW3 wisdom for range of sizes of 2D Complex transform
/// @details This method is used to generate wisdom files. It is not expected to be called in 
/// normal operations when we perform FFTs via operator(). No thread synchronisation is provided
/// in this method (unlike for the rest of the functionality of this class), so it is expected
/// to be used in the serial code. 
/// @note FFTW_WISDOM environment variable must be defined, otherwise an exception is raised.
/// The created wisdom files will have the names required by the wisdom file reading code, provided
/// the value of FFTW_WISDOM variable is the same.
/// @param[in] minSize : smallest size FFT to plan
/// @param[in] maxSize : largest size FFT to plan
/// @param[in] numThreads : number of threads to plan for
/// @param[in] rigor : FFTW plan option (FFTW_MEASURE, FFTW_PATIENT, or FFTW_EXHAUSTIVE)
template<typename FT>
void FFT2DWrapper<FT>::generateWisdom(int minSize, int maxSize, int numThreads, unsigned int rigor)
{
   ASKAPCHECK(minSize > 0, "minSize is expected to be positive, you have "<<minSize);
   ASKAPCHECK(maxSize > 0, "maxSize is expected to be positive, you have "<<maxSize);
   ASKAPCHECK(minSize <= maxSize, "maxSize is expected to be greater than or equal to minSize, you have minSize = "<<minSize<<", maxSize="<<maxSize);
   ASKAPCHECK(numThreads > 0, "numThreads for planning is expected to be positive, you have "<<numThreads);
   // the following is technically unnecessary because an exception will be generated later on, but it is handy to give a useful error message
   ASKAPCHECK(WisdomSingleton::getInstance().wisdomConfigured(), "FFTW_WISDOM environment variable needs to be defined before using generateWisdom");
   
   // setting up planning with the given number of threads, unlike for the main use case of doing FFTs, here we setup
   // planning for the number of threads passed to this method as a parameter rather than via maxThreads. 
   // the following call is no operation if there is no multi-threading support (at all or just in FFTW)
   configurePlanningWithThreads(numThreads);

   casacore::Matrix<FT> data(maxSize,maxSize);
   // helper shared pointer to the start of the array (just to avoid dealing with raw pointers here and make the code nicer)
   // no ownership transfer takes place here
   const typename boost::shared_ptr<typename FT::value_type>
        buf(reinterpret_cast<typename FT::value_type *>(data.data()), utility::NullDeleter());

   // read the current wisdom if there is a file, if present. Note, the file will be overwritten.
   WisdomSingleton& ws = WisdomSingleton::getInstance();
   ws.loadWisdomFileIfNecessary<FT>(numThreads);

   // our logging macro is not very well suited to tcc files (because it tries to create a static and template can be compiled multiple times), so
   // doing the same from first principles and have a local handle variable instead
   log4cxx::LoggerPtr fftlogger = log4cxx::Logger::getLogger(askap::generateLoggerName(std::string(".fft2dwrapper")));
   //
   ASKAPLOG_INFO_STR(fftlogger,"Planning 2D complex<"<<FFTWrapperTypeTraits<FT>::precisionString()<<"> FFT sizes: "<<minSize<<" to "<<maxSize<<" with "<<numThreads<<" threads");

   // the following will not succeed, if FFTW_WISDOM is not defined
   const std::string fileName = ws.wisdomFileName<FT>(numThreads);

   for (int sz = minSize; sz <= maxSize; sz = goodFFTSize(sz + 1)) {
        // get planning done, for both forward and reverse for the given size
        // we don't really need the result. Note, using the proper C++ code will destroy plans
        // pretty much after they're created. It needs to be checked that this doesn't affect wisdom
        // exporting (otherwise, shared pointers to plans need to be added to a container)
        makePlan(casacore::IPosition(2, sz, sz), buf, true, rigor);
        makePlan(casacore::IPosition(2, sz, sz), buf, false, rigor);
        // save results regularly as a check point because plan generation can be a lengthy process
        if (sz % 1024 == 0) {
            WisdomSingleton::writeWisdomFile<FT>(fileName);
            ASKAPLOG_INFO_STR(fftlogger, "Planning complete up to size "<< sz);
        }
    }
    // write again, if there was additional planning past the last check point
    if (maxSize % 1024 != 0) {
        WisdomSingleton::writeWisdomFile<FT>(fileName);
    }
    ASKAPLOG_INFO_STR(fftlogger,"Done - wisdom exported to "<< fileName);
}

} // namespace scimath

} // namespace askap

#endif // #ifndef ASKAP_SCIMATH_FFT_FFT_2D_WRAPPER_TCC
