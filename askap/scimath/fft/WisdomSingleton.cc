/// @file
///
/// This singleton class controls management of FFTW wisdom files (which is a global thing due to
/// the FFTW interface, and hence the use of the singleton pattern). The idea is similar ThreadInitSingleton class, so
/// it was rather tempting just to add the appropriate functionality / methods there. But I (MV) think it is cleaner to 
/// have wisdom files managed by a separate class and avoid fat interfaces / blurring responsibilities between different
/// classes. We decided against clean up of the wisdom information. It is small, cumulative and doesn't need an explicit
/// free up according to the documentation. So, no guard classes similar to those used for threading initialisation are
/// required. However, we reuse the integer tag from the guard classes here to store information per precision option,
/// in a similar fashion to ThreadInitSingleton class. Another difference is the lack of thread synchronisation. 
/// We expect to use this class alongside the plan creation which is already protected by the mutex. So no additional
/// synchronisation is necessary, but care must be taken if this singleton is used directly (rather than through the 
/// FFT wrapper). It looks like FFTW wisdom file is specific to the number of threads used in the parallel case. The main
/// role of this class is to manage this complexity and the two precision options. It is expected that wisdom files 
/// have the name "fftw-[double|float]-Xthreads.wisdom" where X is the number of OpenMP threads without leading zeros.
/// The wisdom files are expected to be located in the directory pointed to by FFTW_WISDOM environment variable. If the
/// variable is not defined, loading wisdom files will be inhibited and FFTs will resort to the usual ESTIMATE method.
/// Otherwise, the loading of wisdom file will be attempted at the beginning and every time the number of available threads
/// changes, separately for each available precision option. If the corresponding file is missing, FFTs will resport to the
/// ESTIMATE method and no further attempts to read this file will be made. If reading the file fails, an exception will be 
/// thrown.
///
/// As for ThreadInitSingleton, the check of the initialisation state is done via the map key. Note, the size of such map 
/// is expected to be very small (only 2 elements in our current use; one element per supported FFTW type), so
/// hopefully no performance impact. If necessary, it can be reimplemented through other means (e.g. a static array).
/// The current way is just rather general and probably results in a more readable code too. 
///
/// This singleton class is used behind the scene by the FFTW wrapper, so the end user should rarely worry about it.
/// For the reference, the typical usage pattern involve calling the static getInstance() method to get the singleton itself and
/// then a templated loadWisdomFileIfNecessary<FT>(numThreads) method for the type of interest (note, typing is done by 
/// the Fourier type, i.e. a complex of some sort) just before plan creation. The parameter passed to the method is the number of
/// available threads.
///
/// \code{.cpp}
///    WisdomSingleton::getInstance().loadWisdomFileIfNecessary<casacore::DComplex>(numThreads);
/// \endcode
///
/// @note The fact that we have to create two separate singleton classes most likely suggests that we a better design could be just
/// one singleton corresponding to the whole FFTW library managing all its global data and precision options. It could have methods
/// returning handler objects for threading or wisdom management as appropriate. We may change the design if there is a need to 
/// integrate more operations like this.
///
///
/// @copyright (c) 2023 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

// own include
#include <askap/scimath/fft/WisdomSingleton.h>
#include <askap/askap/AskapError.h>

#include <askap/askap/AskapLogging.h>
ASKAP_LOGGER(fftlogger, ".fft2dwrapper");


// casa includes
#include <casacore/casa/OS/File.h>
#include <casacore/casa/OS/EnvVar.h>

namespace askap {

namespace scimath {

/// @brief get instance method of the singleton
/// @details As per singleton design pattern with lazy initialisation, an instance is created on the first use
/// @return a non-const reference to the singleton object
WisdomSingleton& WisdomSingleton::getInstance()
{
   if (!theirInstance) {
       // need to initialise the singleton - this is the first use
       theirInstance.reset(new WisdomSingleton);
   }
   return *theirInstance;
}

/// @brief read FFTW_WISDOM environment variable
/// @details This method returns the value of FFTW_WISDOM environment variable.
/// If it is defined, checks are done that the content is a valid path to a directory.
/// @note this method is expected to be called from the private constructor only (although there is no
/// harm doing it in other situations if we'd like). Later on, the result can be accessed via
/// wisdomDirectory method which returns the cached value.
/// @return path to the wisdom directory as indicated by FFTW_WISDOM
std::string WisdomSingleton::obtainWisdomDirectory()
{
   const std::string varName = "FFTW_WISDOM";
   if (casacore::EnvironmentVariable::isDefined(varName)) {
       const std::string wisdomDirPath = casacore::EnvironmentVariable::get(varName);
       // do checks before returning
       const casacore::File file(wisdomDirPath);
       // we could've reused exists method of this class, but it is just a one-liner given that the file object has already been created
       ASKAPCHECK(file.exists(), "Wisdom file directory has been setup but appears to be missing, FFTW_WISDOM = "<<wisdomDirPath);
       ASKAPCHECK(file.isDirectory(), "Wisdom file directory has been setup but FFTW_WISDOM doesn't point to a directory: "<<wisdomDirPath);
       logMessage("Environment variable FFTW_WISDOM is defined, wisdom files to be loaded from "+wisdomDirPath);
       return wisdomDirPath;
   }
   logMessage("Environment variable FFTW_WISDOM not defined - no attempt to load wisdom file(s)");
   return std::string() ;
}

/// @brief helper method to check that the given file exists
/// @details Although we use it only once, it helps with the clarity of the code to have this functionality
/// gathered in a separate method.
/// @param[in] fileName file name to check
/// @return true if the file exists, false otherwise
bool WisdomSingleton::exists(const std::string &fileName)
{
   const casacore::File file(fileName); 
   return file.exists();
}

/// @brief put string message to the log 
/// @details I (MV) found that our logging macros are not very suitable for templated code due to the need to define
/// a global logger instance (templates can be compiled multiple times in different translation units). One can either
/// have a method returning a reference to the logger instantiated in the cc file or a wrapper method like this to 
/// do the logging outside of the template environment. 
/// @param[in] msg string message to put into the log (with INFO severity)
void WisdomSingleton::logMessage(const std::string &msg)
{
   ASKAPLOG_INFO_STR(fftlogger, msg);
   // for debugging, if logging has not been initialised
   //std::cout<<msg<<std::endl;
}

// definition of static data member, doing it in a cc file guarantees to have just one copy in the scimath shared library

/// @brief the key singleton feature - static pointer to the instance
boost::shared_ptr<WisdomSingleton>  WisdomSingleton::theirInstance;

} // namespace scimath

} // namespace askap
