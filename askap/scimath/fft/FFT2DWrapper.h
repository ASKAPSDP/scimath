/// @file
///
/// FFT2D wrapper is an alternative wrapper of FFTW, specific to 2D transforms. It provides similar interface but
/// relies on low-level representation of casacore types. Also it is a bit more careful with locking related to shared
/// memory parallel processing and when the plan is rebuilt.
///
/// @copyright (c) 2022 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_SCIMATH_FFT_FFT_2D_WRAPPER_H
#define ASKAP_SCIMATH_FFT_FFT_2D_WRAPPER_H

// ASKAPsoft includes
#include "askap/scimath/fft/FFTWrapperTypeTraits.h"
#include "askap/scimath/fft/ThreadInitSingleton.h"

// casacore includes
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Array.h>

// boost include
#include "boost/thread/mutex.hpp"
#include "boost/noncopyable.hpp"
#include "boost/shared_ptr.hpp"


namespace askap {

namespace scimath {

// for unit tests
class FFT2DTest;

/// @brief alternative wrapper over FFTW specific to 2D transform
/// @details FFT2D wrapper is an alternative wrapper of FFTW, specific to 2D transforms. It provides a very similar
/// interface (the actual method is the same, but it is a member of the class) but it relies on low-level
/// representation of casacore types. Also it is a bit more careful with locking related to shared
/// memory parallel processing and when the plan is rebuilt. More specifically, the limitations (w.r.t. generic version) are
/// as follows:
///
///    * it does 2D transform only
///    * the interface accepts casacore::Matrix, rather than a generic casacore::Array. For a 2D casacore::Array the users can
///      do conversion (which is cheap), but need to worry about thread safety as appropriate (casa types do reference counting).
///    * the storage should be contiguous (i.e. take care when you do slices of multidimensional casacore::Array)
///    * the plan is cached and built on-demand (i.e. if the sizes change). In the circumstances where two 2D arrays of a
///      different shape are transformed in sequence, it may be practical to do them with different instances of this class.
///
///    In principle, it is possible to make plan cache more advanced and support multiple shapes. This would also enable working with
///    different grid shapes in parallel (current implementation should work in this case too, but will be suboptimal, it is only intended
///    to do different arrays of the same shape in parallel). It is also worth noting that, plans for forward and resverse transforms are
///    cached separately. This is because our typical usage pattern involve forward and reverse FFT in a sequence with some buffer manipulation
///    in between. Such an implementation has some overhead (if only forward or only reverse is used with the particular wrapper), but provides
///    cleaner high level code.
///
template<typename FT>
class FFT2DWrapper : public boost::noncopyable {
public:
   /// @brief constructor
   /// @details The only optional parameter controls harmonic (and origin) location.
   /// @param[in] zeroAtCentre if true, zero harmonics and zero offsets are at the centre of the image
   /// @param[in] maxThreads maximum number of threads to use if built with OpenMP support.
   ///            Negative value (the default) means to take value from omp_get_max_threads() at the time of
   ///            plan creation.
   /// @note Native interface doesn't reorder harmonics which is typically done for presentation purposes.
   /// If the reason for doing FFT is to calculate convolutions between two arrays transformed by the
   /// same procedure, it would be a bit faster to work with native harmonic/origin locations.
   explicit FFT2DWrapper(bool zeroAtCentre = true, int maxThreads = -1) : itsZeroAtCentre(zeroAtCentre), itsMaxThreads(maxThreads)
        { ThreadInitSingleton::getInstance().init<FT>(); }

   /// @brief the main entry point, i.e. method which does 2D FFT
   /// @param[in] data 2D array to transform
   /// @param[in] forward true for a forward transform, false for reverse
   /// @note The method is deliberately made non-const to highlight the fact
   /// that it may rebuild the plan which is cached. Make the whole instance of
   /// the class mutable if you need to have it as a data member of another class and
   /// use it from const methods.
   void operator()(casacore::Matrix<FT> &data, bool forward);

   /// @brief convenience method to transform all hyperplanes of an array serially
   /// @details This method does serial iteration of all hyperplanes (i.e. dimensions beyond the
   /// first two) of a casacore Array by calling operator() in a loop. The iteration is done
   /// serially and a 2D slice is taken meaning that this method is not thread safe
   /// (unlike the main entry point, operator(), which is thread safe). This is not a fundamental
   /// limitation and can probably be removed later on. For 2D arrays it is equivalent to
   /// operator(), except the lack of thread safety.
   /// @param[in] data ND array to transform (N>=2)
   /// @param[in] forward true for a forward transform, false for reverse
   /// @note I (MV) resisted to provide array interface early on because managing thread safety is
   /// difficult this way. But perhaps this convenience method is not a step backwards as long as
   /// we remember that this is only for serial scenarios and the main intention is to reduce code
   /// duplication elsewhere (and remember that it is a bad idea to use it instead of operator() just
   /// to avoid a type cast or changing the code to use the proper type). Each 2D hyperplane of the 
   /// array should be contiguous in memory. It is also possible to 
   /// write an OpenMP version parallelising by hyperplanes. However, it needs some experimentation as
   /// the interaction with OpenMP in the FFTW may be non-trivial.
   void transformAllHyperPlanes(casacore::Array<FT> &data, bool forward);

   
   /// @brief generate FFTW3 wisdom for range of sizes of 2D Complex transform
   /// @details This method is used to generate wisdom files. It is not expected to be called in 
   /// normal operations when we perform FFTs via operator(). No thread synchronisation is provided
   /// in this method (unlike for the rest of the functionality of this class), so it is expected
   /// to be used in the serial code. 
   /// @note FFTW_WISDOM environment variable must be defined, otherwise an exception is raised.
   /// The created wisdom files will have the names required by the wisdom file reading code, provided
   /// the value of FFTW_WISDOM variable is the same.
   /// @param[in] minSize : smallest size FFT to plan
   /// @param[in] maxSize : largest size FFT to plan
   /// @param[in] numThreads : number of threads to plan for
   /// @param[in] rigor : FFTW plan option (FFTW_MEASURE, FFTW_PATIENT, or FFTW_EXHAUSTIVE)
   void generateWisdom(int minSize, int maxSize, int numThreads, unsigned int rigor);

   /// @brief get largest permitted number of OpenMP threads
   /// @details This method returns the smallest of itsMaxThreads, if it is positive and the current value of
   /// omp_get_max_threads() obtained via the LOFAR's wrapper (which returns 1 if no OpenMP is available).
   /// If itsMaxThreads is negative (this is the default), the current value of omp_get_max_threads() is returned.
   /// If the base-scimath library has been compiled without OpenMP support, getMaxThreads method returns 1.
   /// @return maximum number of threads to use for FFT plan execution
   int getMaxThreads() const;
  
protected:
   /// @brief check the plan is suitable and rebuild it if necessary
   /// @details All non-threadsafety is contained in this method and is protected by the mutex.
   /// To simplify this, the interface accepts low-level types instead of extracting this info
   /// from the casacore's array inside (which would complicate thread safety).
   /// @param[in] shape shape of the next array to transform
   /// @param[in] data pointer to the data buffer (in-place transform is assumed)
   /// @param[in] forward true, if the forward transform is to be performed
   /// @return shared pointer to the plan (same as stored in itsPlan, but it is handy to return it as it allows us to
   /// encapsulate all mutex-protected access to caches inside this method
   typename FFTWrapperTypeTraits<FT>::shared_plan_ptr
   rebuildPlanIfNecessary(const casacore::IPosition &shape, const typename boost::shared_ptr<typename FT::value_type> &data, bool forward);

   /// @brief helper method analogous to std::rotate but for a strided sequence
   /// @details We could've written a strided iterator. I (MV) tried to reuse one from
   /// boost but the appropriate module seems to require OpenCL. We also could've only kept
   /// implementation for It == FT*, but doing it this way doesn't hurt performance
   /// @param[in] first iterator to the first element of the sequence
   /// @param[in] middle iterator to the new first element after rotation
   /// @param[in] last end iterator
   /// @param[in] stride stride to add to iterators to advance them.
   /// @note We could've used std::advance but given it is only used with pointer types, it is fine to
   /// have iterators advancing with += operator
   template<typename It>
   static void stridedRotate(It first, It middle, It last, casacore::Int stride);

private:
   /// @brief helper method to alternate signs
   /// @details FFTW doesn't put zero harmonic in the centre. Instead of using more expensive reordering after transform we can multiply
   /// certain elements of the input array by -1 (i.e. flip the sign) in some pattern which depends on the shape of the array. This is more efficient.
   /// @param[in] shape shape of the next array to transform
   /// @param[in] data pointer to the data buffer (in-place transform is assumed)
   /// @note we could've embedded scaling into this procedure too
   static void flipSigns(const casacore::IPosition &shape, const typename boost::shared_ptr<typename FT::value_type> &data);

   /// @brief generalisation of flipSigns approach, works with odd matrix sizes
   /// @details This method implements a general procedure of shifting harmonics via applying a phase gradient in the other domain. It becomes flipSigns
   /// if the matrix has even sizes on both axes.
   /// @param[in] data matrix to work with (modified in situ)
   /// @param[in] forward true for forward transform, false for the reverse
   /// @param[in] afterFFT true for the gradient applied after FFT, false for the gradient applied before FFT (matters in the odd size case)
   static void applyPhaseGradient(casacore::Matrix<FT> &data, bool forward, bool afterFFT);

   /// @brief harmonic/data rearrangement via direct data move
   /// @details This method serves the same purpose as applyPhaseGradient (or flipSigns) but does the operation via moving the data in the original domain
   /// (instead of the phase gradient, or its degenerate version = the sign flips, in the other domain)
   /// @param[in] data matrix to work with (modified in situ)
   /// @param[in] afterFFT true if run after FFT in our case
   static void fftShift(casacore::Matrix<FT> &data, bool afterFFT);

   /// @brief helper method to make the plan
   /// @details This method encapsulates interface differences for different types (FT), it is called from rebuildPlanIfNecessary
   /// @param[in] shape shape of the next array to transform
   /// @param[in] data pointer to the data buffer (in-place transform is assumed)
   /// @param[in] forward true, if the forward transform is to be performed
   /// @param[in] rigor planning-rigor flags which can be or'ed (e.g. FFTW_ESTIMATE). Note, only FFTW_ESTIMATE and FFTW_WISDOM_ONLY can be used
   ///            in normal operations via rebuildPlanIfNecessary as other planning methods overwrite the input array. We can use other flags 
   ///            when generating wisdom files.
   /// @return shared pointer to the plan
   typename FFTWrapperTypeTraits<FT>::shared_plan_ptr
   makePlan(const casacore::IPosition &shape, const typename boost::shared_ptr<typename FT::value_type> &data, bool forward, unsigned int rigor);

   /// @brief helper method to get alignment of the buffer
   /// @details This is to ensure we always use high-performance algorithm underneath (although on modern machines it may be possible to short cut these
   /// checks)
   /// @param[in] data pointer to the data buffer
   /// @return integer alignment info as returned by FFTW
   static int getAlignment(const typename boost::shared_ptr<typename FT::value_type> &data);

   /// @brief execute plan
   /// @details Wrapper around FFTW methods executing the plan
   /// @param[in] plan plan to use
   /// @param[in] data pointer to the data buffer
   static void executePlan(const typename FFTWrapperTypeTraits<FT>::shared_plan_ptr &plan,
                           const typename boost::shared_ptr<typename FT::value_type> &data);

   /// @brief low-level method to enable planning with threads
   /// @details This has to be called before making a plan or loading wisdom file if using 
   /// OpenMP threads. The desired number of threads is the parameter. In normal operations, we pass
   /// the number of threads returned by getMaxThreads. This method just calls the FFTW routine 
   /// appropriate for the data type. If the library is compiled without OpenMP support (at all or
   /// just without OpenMP-enabled FFT), this method does nothing (call to FFTW is not compiled as
   /// the appropriate FFTW routine may not even exist).
   /// @note In normal operations this method is called inside rebuildPlanIfNecessary and is protected by
   /// mutex, so no additional synchronisation is necessary. We also use it in wisdom generation, but
   /// this is not expected to be done in parallel at the moment.
   /// @param[in] numThreads number of threads to use for the planning
   static void configurePlanningWithThreads(int numThreads);

   /// @brief helper method to create shared pointer to a plan
   /// @details It helps not to forget PlanDeleter + constructs the right type and provides synchronisation using 
   /// the same mutex as used in plan creation.
   /// @param[in] plan plan returned by an FFTW routine 
   /// @return shared pointer to the plan
   typename FFTWrapperTypeTraits<FT>::shared_plan_ptr makeSharedPlanPtr(typename FFTWrapperTypeTraits<FT>::plan_type plan);

   /// @brief mutex to protect plan creation and related checks
   /// @note we rely on the fact that itsMutex is declared before itsPlan (and, therefore, destructed in the reverse order)
   /// so plan destruction can be done sequentially with locking making it threadsafe. I (MV) struggle to decide whether this is
   /// an acceptable solution or having mutex held by a shared pointer would be better (although, perhaps, messier code-wise).
   /// If the order is reversed, access to unallocated object / segfaults may follow. On the other hand, the destruction will
   /// most likely happen outside of the parallel environment anyway, so protecting plan destruction by a mutex may be a bit of perfectionism.
   boost::mutex itsMutex;

   /// @brief helper method to translate forward flag into index
   /// @details We cache plans for both forward and inverse to simplify usage (although there is a minor waste of resources).
   /// This is because we usually do forward and reverse transforms on grids of the same size in sequence. Plan, shape and
   /// alignment info are stored in 2-element array (zero-th element is for reverse and the first is for the forward transform).
   /// This method helps translating boolean flag (we could've just cast the type, but doing it this way is more clean).
   /// @param[in] forward true, if the forward transform is requested
   /// @return 0 for reverse transform, 1 for the forward one
   static inline int directionIndex(bool forward) { return forward ? 1 : 0; }

   /// @brief size for which the plan is initialised
   /// @note A zero-length IPosition means no plan has been initialised yet
   casacore::IPosition itsPlanShape[2];

   /// @brief cached alignment of the array the plan was created for
   /// @details This is used for cross-check or to rebuild plan if we ever have a mismatch in alignment
   /// because casacore doesn't use FFTW's memory allocation routine
   int itsPlanAlignment[2];

   /// @brief cached plan
   typename FFTWrapperTypeTraits<FT>::shared_plan_ptr itsPlan[2];

   /// @brief if true, zero harmonics and zero offsets are at the centre of the image
   /// @note Native interface doesn't reorder harmonics which is typically done for presentation purposes.
   /// If the reason for doing FFT is to calculate convolutions between two arrays transformed by the
   /// same procedure, it would be a bit faster to work with native harmonic/origin locations.
   const bool itsZeroAtCentre;

   /// @brief maximum number of OpenMP threads to use if multithreading support is enabled
   /// @details A non-positive value means to use the current value of omp_get_max_threads which is probed at the
   /// time of plan creation (i.e. the first transform with the given shape, direction and alignment done via the
   /// current wrapper)
   /// @note the data member has been made const because we currently don't envisage changing it during the life of
   /// the wrapper object. It can be removed later on if modification is needed (although care must be taken / potential
   /// technical debt may arise in regards to the moment when the change comes in force)
   const int itsMaxThreads;

   // for unit tests
   friend FFT2DTest;
};

} // namespace scimath

} // namespace askap

#include <askap/scimath/fft/FFT2DWrapper.tcc>

#endif // #ifndef ASKAP_SCIMATH_FFT_FFT_2D_WRAPPER_H
