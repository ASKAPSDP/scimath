/// @file
///
/// Test of EstimatorAdapter template wrapping up a custom "estimator" class into normal equations 
///
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#include <cppunit/extensions/HelperMacros.h>

#include <askap/scimath/utils/ChangeMonitor.h>
#include <askap/scimath/utils/EstimatorAdapter.h>

#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

#include <boost/noncopyable.hpp>

namespace askap {

namespace scimath {

class EstimatorAdapterTest : public CppUnit::TestFixture 
{
   CPPUNIT_TEST_SUITE(EstimatorAdapterTest);
   CPPUNIT_TEST(testCreate);
   CPPUNIT_TEST(testReset);
   CPPUNIT_TEST(testSerialization);
   CPPUNIT_TEST(testMerge);
   CPPUNIT_TEST(testMonitor);
   CPPUNIT_TEST(testBuildNonCopyable);
   CPPUNIT_TEST_SUITE_END();

   /// @brief tester class (acts as an estimator)
   struct TestEstimator {
  
      explicit TestEstimator(const std::string &name) : itsName(name) {}

      /// @brief reset method,just notify change monitor
      void reset() { itsName = "reset"; itsChangeMonitor.notifyOfChanges(); }

      /// @brief write the object to a blob stream
      /// @param[in] os the output stream
      void writeToBlob(LOFAR::BlobOStream& os) const
        { os << itsName; }

      /// @brief read the object from a blob stream
      /// @param[in] is the input stream
      /// @note Not sure whether the parameter should be made const or not
      void readFromBlob(LOFAR::BlobIStream& is)
        { is >> itsName; }

      void merge(const TestEstimator &other) {
           itsName += "+" + other.itsName;
           itsChangeMonitor.notifyOfChanges();
      }

      /// @brief change monitor to be used in test conditions
      ChangeMonitor itsChangeMonitor;

      /// @brief name to distinguish one object of this type from another
      std::string itsName;
   };

   struct TestMonitor {
      static inline void aboutToMerge(const TestEstimator &first, const TestEstimator &second) {
         theirString = first.itsName+"_"+second.itsName;
      };
      static std::string theirString;
   };

    struct TestEstimatorNonCopyable : public TestEstimator,
                                      public boost::noncopyable {
       TestEstimatorNonCopyable() : TestEstimator("noncopyable_estimator") {}
    };
public:

   void testBuildNonCopyable() {
      // create a test estimator
      boost::shared_ptr<TestEstimatorNonCopyable> estimator(new TestEstimatorNonCopyable());
      CPPUNIT_ASSERT(estimator);
      // wrap it in an adapter
      EstimatorAdapter<TestEstimatorNonCopyable> adapter(estimator);
      CPPUNIT_ASSERT_EQUAL(estimator, adapter.get());
   }

   void testCreate() {
      // create a test estimator
      boost::shared_ptr<TestEstimator> estimator(new TestEstimator("original"));
      CPPUNIT_ASSERT(estimator);
      // wrap it in an adapter
      EstimatorAdapter<TestEstimator> adapter(estimator);
      CPPUNIT_ASSERT_EQUAL(estimator, adapter.get());
      // clone it via NE interface
      INormalEquations::ShPtr clonePtr = adapter.clone();
      // cast to the actual type
      boost::shared_ptr<EstimatorAdapter<TestEstimator> > clonePtr2 = dynamic_pointer_cast<EstimatorAdapter<TestEstimator> >(clonePtr);
      CPPUNIT_ASSERT(clonePtr2);
      // test that the clone points to the same thing
      CPPUNIT_ASSERT(clonePtr2->get() != estimator);
      CPPUNIT_ASSERT_EQUAL(estimator->itsName, clonePtr2->get()->itsName);
      CPPUNIT_ASSERT_EQUAL(std::string("original"), clonePtr2->get()->itsName);
   }

   void testReset() {
      // create a test estimator
      boost::shared_ptr<TestEstimator> estimator(new TestEstimator("original"));
      CPPUNIT_ASSERT(estimator);
      // obtain the change monitor to verify change of state later
      const ChangeMonitor cm = estimator->itsChangeMonitor;
      // wrap the test estimator in an adapter
      EstimatorAdapter<TestEstimator> adapter(estimator);
      CPPUNIT_ASSERT(adapter.get());
      CPPUNIT_ASSERT_EQUAL(std::string("original"), adapter.get()->itsName);
      // reset the estimator via NE interface using the adapter
      adapter.reset();
      // check that the reset action took place
      CPPUNIT_ASSERT_EQUAL(std::string("reset"), adapter.get()->itsName);
      CPPUNIT_ASSERT_EQUAL(std::string("reset"), estimator->itsName);
      CPPUNIT_ASSERT(cm != estimator->itsChangeMonitor);
   }

   void testSerialization() {
      // create a test estimator
      boost::shared_ptr<TestEstimator> estimator(new TestEstimator("source"));
      CPPUNIT_ASSERT(estimator);
      // wrap the test estimator in an adapter
      EstimatorAdapter<TestEstimator> adapter(estimator);
      CPPUNIT_ASSERT(adapter.get());
      CPPUNIT_ASSERT_EQUAL(std::string("source"), adapter.get()->itsName);
      // serialise into blob string via generic NE interface
      LOFAR::BlobString bs;
      LOFAR::BlobOBufString bob(bs);
      LOFAR::BlobOStream bos(bob);
      adapter.writeToBlob(bos);
      // another instance to hold the result
      boost::shared_ptr<TestEstimator> anotherEstimator(new TestEstimator("junk"));
      CPPUNIT_ASSERT(anotherEstimator);
      EstimatorAdapter<TestEstimator> anotherAdapter(anotherEstimator);
      CPPUNIT_ASSERT(anotherAdapter.get());
      // reset it (although technically not needed for this test)
      anotherAdapter.reset();
      CPPUNIT_ASSERT_EQUAL(std::string("reset"), anotherAdapter.get()->itsName);

      // de-serialise from the blob string
      LOFAR::BlobIBufString bib(bs);
      LOFAR::BlobIStream bis(bib);
      anotherAdapter.readFromBlob(bis);

      // check the result
      CPPUNIT_ASSERT_EQUAL(std::string("source"), anotherAdapter.get()->itsName);
   }

   void testMerge() {
      // create a test estimator
      boost::shared_ptr<TestEstimator> estimator(new TestEstimator("source"));
      CPPUNIT_ASSERT(estimator);
      // wrap the test estimator in an adapter
      EstimatorAdapter<TestEstimator> adapter(estimator);
      CPPUNIT_ASSERT(adapter.get());
      CPPUNIT_ASSERT_EQUAL(std::string("source"), adapter.get()->itsName);

      // another test estimator
      boost::shared_ptr<TestEstimator> estimator2(new TestEstimator("destination"));
      CPPUNIT_ASSERT(estimator2);
      // obtain the change monitor to verify change of state later
      const ChangeMonitor cm = estimator2->itsChangeMonitor;

      // wrap it in an adapter
      EstimatorAdapter<TestEstimator> adapter2(estimator2);
      CPPUNIT_ASSERT(adapter2.get());
      CPPUNIT_ASSERT_EQUAL(std::string("destination"), adapter2.get()->itsName);

      // call merge via the NE interface
      adapter2.merge(adapter);

      // test the result
      CPPUNIT_ASSERT_EQUAL(std::string("destination+source"), adapter2.get()->itsName);
      CPPUNIT_ASSERT(adapter2.get()->itsChangeMonitor != cm);
   }

   void testMonitor() {
      // create a test estimator
      boost::shared_ptr<TestEstimator> estimator(new TestEstimator("source"));
      CPPUNIT_ASSERT(estimator);
      // wrap the test estimator in an adapter
      EstimatorAdapter<TestEstimator, TestMonitor> adapter(estimator);

      // another test estimator
      boost::shared_ptr<TestEstimator> estimator2(new TestEstimator("destination"));
      CPPUNIT_ASSERT(estimator2);

      // wrap it in an adapter
      EstimatorAdapter<TestEstimator, TestMonitor> adapter2(estimator2);
      // call merge via the NE interface
      adapter2.merge(adapter);

      // test the variable in the monitoring class
      CPPUNIT_ASSERT_EQUAL(std::string("destination_source"), TestMonitor::theirString);
   }
};

/// @brief ideally we need to put it into a cc file, but this header is included only once
std::string EstimatorAdapterTest::TestMonitor::theirString;
    
} // namespace scimath

} // namespace askap

