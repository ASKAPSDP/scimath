/// @file
///
/// @brief test of array math utilities
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

// unit test include
#include <cppunit/extensions/HelperMacros.h>

// own includes
#include "askap/scimath/utils/OptimizedArrayMathUtils.h"

namespace askap {

namespace utility {


class OptimizedArrayMathUtilsTest : public CppUnit::TestFixture 
{
   CPPUNIT_TEST_SUITE(OptimizedArrayMathUtilsTest);
   CPPUNIT_TEST(testMultiplyByConjugate);
   CPPUNIT_TEST_EXCEPTION(testMultiplyByConjugateNotConformant, AskapError);     
   CPPUNIT_TEST_EXCEPTION(testMultiplyByConjugateNotContiguous, AskapError);     
   CPPUNIT_TEST(testConjugateComplexArray);
   CPPUNIT_TEST_EXCEPTION(testConjugateComplexArrayNotContiguous, AskapError);
   CPPUNIT_TEST(testCalculateNormalisedProduct);
   CPPUNIT_TEST(testCalculateNormalisedProductNotContiguous);
   CPPUNIT_TEST(testCalculateNormalisedProductNotConformant);
   CPPUNIT_TEST(testRealPartMean);
   CPPUNIT_TEST_EXCEPTION(testRealPartMeanNotContiguous, AskapError);
   CPPUNIT_TEST(testAddGaussian);
   CPPUNIT_TEST_EXCEPTION(testAddGaussianNotContiguous, AskapError);
   CPPUNIT_TEST_SUITE_END();
protected:
   void testMultiplyByConjugate() {
       testMultiplyByConjugateImpl<casacore::Complex>();
       testMultiplyByConjugateImpl<casacore::DComplex>();
   }

   void testConjugateComplexArray() {
       testConjugateComplexArrayImpl<casacore::Complex>();
       testConjugateComplexArrayImpl<casacore::DComplex>();
   }

   void testMultiplyByConjugateNotConformant() {
        casacore::Matrix<casacore::Complex> mtr1(64., 64., casacore::Complex(1.,0.));
        casacore::Matrix<casacore::Complex> mtr2(32., 128., casacore::Complex(1.,0.));
        // an exception should be thrown
        multiplyByConjugate(mtr1, mtr2);
   }

   void testMultiplyByConjugateNotContiguous() {
        casacore::Matrix<casacore::Complex> mtr1(64., 64., casacore::Complex(1.,0.));
        casacore::Cube<casacore::Complex> cube(64., 64., 8., casacore::Complex(1.,0.));

        casacore::Matrix<casacore::Complex> mtr2(cube.yzPlane(0));
        // an exception should be thrown because mtr2 doesn't span contiguous memory (xyPlane(..) would be ok);
        multiplyByConjugate(mtr1, mtr2);
   }

   void testConjugateComplexArrayNotContiguous() {
        casacore::Cube<casacore::Complex> cube(64., 64., 8., casacore::Complex(1.,0.));
        casacore::Matrix<casacore::Complex> mtr(cube.yzPlane(0));
        // an exception should be thrown because mtr doesn't span contiguous memory (xyPlane(..) would be ok);
        conjugateComplexArray(mtr);
   }

   void testCalculateNormalisedProduct() {
        testCalculateNormalisedProductImpl<casacore::Complex>();
        testCalculateNormalisedProductImpl<casacore::DComplex>();
   }
  
   void testCalculateNormalisedProductNotContiguous() {
        casacore::Cube<casacore::Complex> cube(64, 64, 8, casacore::Complex(1.,0.));
        casacore::Matrix<casacore::Complex> mtrNonContiguous(cube.yzPlane(0));
        casacore::Matrix<casacore::Complex> mtrContiguous(cube.xyPlane(0));
        casacore::Array<casacore::Complex> result(mtrContiguous.shape());
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(result, mtrNonContiguous, mtrContiguous, mtrContiguous, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(result, mtrContiguous, mtrNonContiguous, mtrContiguous, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(result, mtrContiguous, mtrContiguous, mtrNonContiguous, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(mtrNonContiguous, mtrContiguous, mtrContiguous, mtrContiguous, 1.f), AskapError);
   }

   void testCalculateNormalisedProductNotConformant() {
        casacore::Matrix<casacore::Complex> mtr1(64, 32, casacore::Complex(1.,0.));
        casacore::Matrix<casacore::Complex> mtr2(128, 64, casacore::Complex(1.,0.));
        casacore::Matrix<casacore::Complex> result(mtr1.shape());
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(result, mtr2, mtr1, mtr1, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(result, mtr1, mtr2, mtr1, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(result, mtr1, mtr1, mtr2, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(mtr2, mtr1, mtr1, mtr1, 1.f), AskapError);
        CPPUNIT_ASSERT_THROW(calculateNormalisedProduct(mtr2, mtr1, mtr1, mtr2, 1.f), AskapError);
   }

   void testRealPartMean() {
        testRealPartMeanImpl<casacore::Complex>();
        testRealPartMeanImpl<casacore::DComplex>();
   }
   
   void testRealPartMeanNotContiguous() {
        casacore::Cube<casacore::Complex> cube(64., 64., 8., casacore::Complex(1.,0.));
        casacore::Matrix<casacore::Complex> mtr(cube.yzPlane(0));
        // an exception should be thrown because mtr doesn't span contiguous memory (xyPlane(..) would be ok);
        realPartMean(mtr);
   }

   void testAddGaussianNotContiguous() {
        casacore::Cube<float> cube(64., 64., 8., 1.f);
        casacore::Matrix<float> mtr(cube.yzPlane(0));
        // an exception should be thrown because mtr doesn't span contiguous memory (xyPlane(..) would be ok);
        addGaussian(mtr, 2.f, 1.1f, 0.f, 3.f, 5.f);
   }

   void testAddGaussian() {
        testAddGaussianImpl<casacore::Complex>();
        testAddGaussianImpl<casacore::DComplex>();
        testAddGaussianImpl<float, float>();
        testAddGaussianImpl<double, double>();
   }

   // actual implementation of tests which are intended to be run in both single and double precision
   
   template<typename FT, typename T = typename FT::value_type>
   void testAddGaussianImpl() {
       const casa::uInt size = 128;
       const T row_off(7.5);
       const T col_off(-4);
       const T row_width(10);
       const T col_width(5);
       const T peak(120.);
       casacore::Matrix<FT> buffer(size, size, static_cast<T>(0.1));
       casacore::Matrix<FT> buffer2 = buffer.copy();
       // fill from first principles
       for (casacore::Int row = 0; row < buffer.nrow(); ++row) {
            const T part = casacore::square((row - static_cast<T>(buffer.nrow()) / 2 - static_cast<T>(row_off)) / row_width);
            for (casacore::Int col = 0; col < buffer.ncolumn(); ++col) {
                 const T rad2 =  part + casacore::square((col - static_cast<T>(buffer.ncolumn()) / 2 - static_cast<T>(col_off)) / col_width);
                 buffer(row,col) += peak * exp(-4.*log(2.) * rad2);
            }
       }
       // fill a second buffer via the addGaussian method
       addGaussian(buffer2, peak, row_off, col_off, row_width, col_width);
       // check that they match within tolerance
       CPPUNIT_ASSERT_EQUAL(buffer.nrow(), buffer2.nrow());
       CPPUNIT_ASSERT_EQUAL(buffer.ncolumn(), buffer2.ncolumn());
       CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), buffer.nrow());
       CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), buffer.ncolumn());
       for (casacore::uInt row = 0; row < buffer.nrow(); ++row) {
            for (casacore::uInt col = 0; col < buffer.ncolumn(); ++col) {
                 const T diff = casacore::abs(buffer(row,col) - buffer2(row,col));
                 CPPUNIT_ASSERT_DOUBLES_EQUAL(static_cast<T>(0.), diff, 1e-6);
            }
       }
   }
  
   template<typename FT>
   void testMultiplyByConjugateImpl() {
        const casa::uInt size = 128;
        casacore::Matrix<FT> mtr(size,size,FT(1.,-1.));
        casacore::Matrix<FT> other(size,size, FT(2.,0.5));
        for (casacore::uInt row = 0; row < other.nrow(); ++row) {
             for (casacore::uInt col = 0; col < other.ncolumn(); ++col) {
                  other(row,col) = FT(row * col, static_cast<typename FT::value_type>(0.5) * row *col);
             }
        }
        casacore::Matrix<FT> mtrCopy(mtr.copy());

        mtrCopy *= conj(other);
        multiplyByConjugate(mtr, other);

        for (casacore::uInt row = 0; row < mtr.nrow(); ++row) {
             for (casacore::uInt col = 0; col < mtr.ncolumn(); ++col) {
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(real(mtrCopy(row,col)), real(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(imag(mtrCopy(row,col)), imag(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(static_cast<typename FT::value_type>(0.5)*row*col, real(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(static_cast<typename FT::value_type>(-1.5)*row*col, imag(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
             }
        }
   }
   
   template<typename FT>
   void testConjugateComplexArrayImpl() {
        const casa::uInt size = 128;
        casacore::Matrix<FT> mtr(size,size,FT(1.,-1.));
        for (casacore::uInt row = 0; row < mtr.nrow(); ++row) {
             for (casacore::uInt col = 0; col < mtr.ncolumn(); ++col) {
                  mtr(row,col) = FT(static_cast<typename FT::value_type>(1.)*row * col * (row%2 == 0 ? 1 : -1), 
                                    static_cast<typename FT::value_type>(0.5) * row *col * (col%2 == 0 ? 1 : -1));
             }
        }
        casacore::Matrix<FT> mtrCopy(conj(mtr));
        conjugateComplexArray(mtr);
        for (casacore::uInt row = 0; row < mtr.nrow(); ++row) {
             for (casacore::uInt col = 0; col < mtr.ncolumn(); ++col) {
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(real(mtrCopy(row,col)), real(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(imag(mtrCopy(row,col)), imag(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(static_cast<typename FT::value_type>(1.)*row*col*(row%2 == 0 ? 1 : -1), real(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(static_cast<typename FT::value_type>(0.5)*row*col*(col%2 == 0 ? -1 : 1), imag(mtr(row,col)), static_cast<typename FT::value_type>(2e-7)*row*col);
             }
        }
   }

   template<typename FT>
   void testCalculateNormalisedProductImpl() {
        const casa::uInt sizeX = 128;
        const casa::uInt sizeY = 64;
        casacore::Matrix<FT> mtrA(sizeX,sizeY);
        casacore::Matrix<FT> mtrB(sizeX,sizeY);
        // add a degenerate dimension to test it too
        casacore::Array<FT> arrC(casacore::IPosition(3, sizeX, sizeY, 1), FT(1.,-1.));
        for (casacore::uInt row = 0; row < mtrA.nrow(); ++row) {
             for (casacore::uInt col = 0; col < mtrA.ncolumn(); ++col) {
                  mtrA(row,col) = FT(static_cast<typename FT::value_type>(1.)*row * col * (row%2 == 0 ? 1 : -1), 
                                    static_cast<typename FT::value_type>(0.5) * row *col * (col%2 == 0 ? 1 : -1));
                  mtrB(row,col) = FT(static_cast<typename FT::value_type>(0.5)*row * col, static_cast<typename FT::value_type>(1.) * row *col);
             }
        }
        casacore::Array<FT> result1(arrC.shape()), result2(arrC.shape());
        const typename FT::value_type normFactor = 2.;

        calculateNormalisedProduct(result1, mtrA, mtrB, arrC, normFactor);
        result2.nonDegenerate(2) = conj(mtrA) * mtrB * arrC.nonDegenerate(2) / normFactor; 

        
        for (casacore::uInt row = 0; row < mtrA.nrow(); ++row) {
             for (casacore::uInt col = 0; col < mtrA.ncolumn(); ++col) {
                  const casacore::IPosition curPos(3,row,col,0);
                  const FT val1 = result1(curPos);
                  const FT val2 = result2(curPos);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(real(val1), real(val2), static_cast<typename FT::value_type>(2e-7)*row*col);
                  CPPUNIT_ASSERT_DOUBLES_EQUAL(imag(val1), imag(val2), static_cast<typename FT::value_type>(2e-7)*row*col);
             }
        }
   }

   template<typename FT> 
   void testRealPartMeanImpl() {
        const casa::uInt sizeX = 128;
        const casa::uInt sizeY = 64;
        casacore::Matrix<FT> mtrA(sizeX,sizeY);
        for (casacore::uInt row = 0; row < mtrA.nrow(); ++row) {
             for (casacore::uInt col = 0; col < mtrA.ncolumn(); ++col) {
                  mtrA(row,col) = FT(static_cast<typename FT::value_type>(1.)*row * col * (row%2 == 0 ? 1 : -1), 
                                    static_cast<typename FT::value_type>(0.5) * row *col * (col%2 == 0 ? 1 : -1));
             }
        }
        casacore::Array<FT> arr(mtrA);

        const typename FT::value_type mean1 = realPartMean(arr);
        const typename FT::value_type mean2 = casacore::sum(casacore::real(mtrA)) / mtrA.nelements();

        CPPUNIT_ASSERT_DOUBLES_EQUAL(mean2, mean1, static_cast<typename FT::value_type>(2e-7) * mtrA.nelements()); 
   }
};

} // namespace utility

} // namespace askap

