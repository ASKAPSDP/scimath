/// @file ffttest2.cc
///
/// @brief More advanced performance test for various FFT routines
/// @details Unlike ffttest.cc, which is designed to be extremely simple test not depending on
/// our build system or a large suite of 3rd party libraries, this test uses our wrappers and
/// the wrapper provided by casacore's Lattice interface to be a more representative of what we have
/// in the production code. 
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

// boost includes
#include <boost/noncopyable.hpp>

// std includes
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

// casacore includes
#include <casacore/casa/BasicSL/Complex.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/lattices/Lattices/ArrayLattice.h>
#include <casacore/lattices/LatticeMath/LatticeFFT.h>
#include <casacore/casa/OS/Timer.h>
#include <casacore/casa/OS/File.h>

// lofar (for OpenMP wrapper)
#include <Common/OpenMP.h>

// own includes
#include <askap/scimath/fft/FFT2DWrapper.h>
#include <askap/askap/AskapError.h>
#include <askap/askap/AskapLogging.h>
#include <askap/scimath/fft/FFTWrapper.h>
#include <askap/scimath/utils/OptimizedArrayMathUtils.h>

ASKAP_LOGGER(logger, "scimath.fft");

using namespace askap;

/// @brief helper template to assist with comparison of single and double precision tests
/// @details By default, no members are defined, so compilation would fail
template<typename FT>
struct TheOtherPrecisionOption {
};

template<>
struct TheOtherPrecisionOption<casacore::Complex> {
   typedef casacore::DComplex value;
   typedef casacore::DComplex double_precision;
};

template<>
struct TheOtherPrecisionOption<casacore::DComplex> {
   typedef casacore::Complex value;
   typedef casacore::DComplex double_precision;
};

/// @brief generic interface of a tester class
struct ITester : public boost::noncopyable {
  /// @brief virtual destructor to keep the compiler happy
  virtual ~ITester() {}

  /// @brief compare results with some other tester
  /// @details Different methods may have different accuracy, this method
  /// compares matrices held by two different testers and returns the largest
  /// difference by absolute value. It makes sense to run it only after test
  /// (although no harm comparing matrices after initialisation).
  /// @note an exception is thrown if test matrix has not been initialised in either of
  /// the class or sizes differ. Also, due to type conversion, the operation may not commute.
  /// @brief other const reference to an instance to compare with
  /// @return largest by absolute value difference between two matrices
  virtual double compare(const ITester &other) const = 0;

  /// @brief initialise buffers
  /// @param[in] nRow number of rows in the buffer
  /// @param[in] nColumn number of columns in the buffer
  virtual void init(casacore::uInt nRow, casacore::uInt nColumn) = 0;

  /// @brief run the test
  /// @details It is handy to confine the actual test execution to the single method to make it easier to time it.
  /// @note an exception may be thrown if the test has not been initialised
  virtual void run() = 0;
};

/// @brief tester class with the buffer of the type FT
template<typename FT>
struct TesterBase : public ITester {
   /// @brief initialise buffers
   /// @param[in] nRow number of rows in the buffer
   /// @param[in] nColumn number of columns in the buffer
   virtual void init(casacore::uInt nRow, casacore::uInt nColumn) final {
      itsBuffer.resize(nRow, nColumn);
      itsBuffer.set(FT(0., 0.));
      utility::addGaussian(itsBuffer, static_cast<typename FT::value_type>(1.), static_cast<typename FT::value_type>(10.5), 
                  static_cast<typename FT::value_type>(-13.1), static_cast<typename FT::value_type>(4.0), static_cast<typename FT::value_type>(5.5)); 
   }

   /// @brief compare results with some other tester
   /// @details Different methods may have different accuracy, this method
   /// compares matrices held by two different testers and returns the largest
   /// difference by absolute value. It makes sense to run it only after test
   /// (although no harm comparing matrices after initialisation).
   /// @note an exception is thrown if test matrix has not been initialised in either of
   /// the class or sizes differ. Also, due to type conversion, the operation may not commute.
   /// @param[in] other const reference to an instance to compare with
   /// @return largest by absolute value difference between two matrices
   virtual double compare(const ITester &other) const final {
      try {
         const TesterBase<FT>& castOther = dynamic_cast<const TesterBase<FT>&>(other);
         return compare(itsBuffer, castOther.itsBuffer);
      }
      catch (const std::bad_cast&) {}
      // if it gets here we're trying to compare single and double precision complex
      typedef typename TheOtherPrecisionOption<FT>::value other_type;
      // deliberately don't do try-catch block here - if it fails to cast, we don't have this case implemented (e.g. long double, custom types)
      const TesterBase<other_type>& castOther = dynamic_cast<const TesterBase<other_type>&>(other);
      ASKAPCHECK(itsBuffer.shape() == castOther.buffer().shape(), "Grid sizes are expected to be the same for all tests");
      double largestDiff = 0;
      #pragma omp parallel for reduction(max:largestDiff)
      for (casacore::uInt row = 0; row < itsBuffer.nrow(); ++row) {
           for (casacore::uInt col = 0; col < itsBuffer.nrow(); ++col) {
                typedef typename TheOtherPrecisionOption<FT>::double_precision dcomplex;
                const dcomplex temp = static_cast<dcomplex>(itsBuffer(row,col)) - static_cast<dcomplex>(castOther.buffer()(row,col));
                const double tempAmp = casacore::abs(temp);
                if (largestDiff < tempAmp) {
                    largestDiff = tempAmp;
                }
           }
      }
      return largestDiff;
   }

   /// @brief obtain reference to the buffer
   /// @return reference to the buffer
   casacore::Matrix<FT>& buffer() { return itsBuffer; }

   /// @brief obtain reference to the buffer
   /// @return reference to the buffer
   const casacore::Matrix<FT>& buffer() const { return itsBuffer; }
private:
   /// @brief actual implementation of the comparison
   /// @param[in] first const reference to the first matrix to compare
   /// @param[in] second const reference to the second matrix to compare
   /// @return largest by absolute value difference between two matrices
   static double compare(const casacore::Matrix<FT> &first, const casacore::Matrix<FT> &second) {
       ASKAPCHECK(first.shape() == second.shape(), "An attempt to compare matrices of different shape: "<<
                  first.shape()<<" and "<<second.shape());
       return casacore::max(casacore::amplitude(first - second));
   }

   /// @brief buffer 
   casacore::Matrix<FT> itsBuffer;
};

/// @brief tester for the new wrapper
template<typename FT>
struct FFT2DWrapperTester : public TesterBase<FT> {
  /// @brief run the test
  /// @details It is handy to confine the actual test execution to the single method to make it easier to time it.
  /// @note an exception may be thrown if the test has not been initialised
  virtual void run() final {
     // reorder harmonics as for the other wrappers
     // technically, we could've overriden init and have the wrapper initialised there, but it gets too complicated and the difference is tiny anyway
     scimath::FFT2DWrapper<FT> fft2d(true);
     fft2d(this->buffer(), true);
  }
};

/// @brief tester for the old wrapper
template<typename FT>
struct FFTWrapperTester : public TesterBase<FT> {
  /// @brief run the test
  /// @details It is handy to confine the actual test execution to the single method to make it easier to time it.
  /// @note an exception may be thrown if the test has not been initialised
  virtual void run() final {
     scimath::fft2d(this->buffer(), true);
  }
};

/// @brief tester for the casacore code
template<typename FT>
struct LatticeFFTTester : public TesterBase<FT> {
  /// @brief run the test
  /// @details It is handy to confine the actual test execution to the single method to make it easier to time it.
  /// @note an exception may be thrown if the test has not been initialised
  virtual void run() final {
     casacore::ArrayLattice<FT> lattice(this->buffer());
     casacore::LatticeFFT::cfft2d(lattice, true);
  }
};

/// @brief factory method for various tests

/// @brief factory method for various tests
/// @details This method encapsulates creation of various tests referred to by the string name
/// @param[in] name name of the test (e.g. new_wrapper_flt, new_wrapper_dbl)
boost::shared_ptr<ITester> testFactory(const std::string &name) {
  boost::shared_ptr<ITester> tester;
  if (name == "new_wrapper_flt") {
      tester.reset(new FFT2DWrapperTester<casacore::Complex>());
  } else if (name == "new_wrapper_dbl") {
      tester.reset(new FFT2DWrapperTester<casacore::DComplex>());
  } else if (name == "old_wrapper_flt") {
      tester.reset(new FFTWrapperTester<casacore::Complex>());
  } else if (name == "old_wrapper_dbl") {
      tester.reset(new FFTWrapperTester<casacore::DComplex>());
  } else if (name == "latticefft_flt") {
      tester.reset(new LatticeFFTTester<casacore::Complex>());
  } else if (name == "latticefft_dbl") {
      tester.reset(new LatticeFFTTester<casacore::DComplex>());
  }
  ASKAPCHECK(tester, "Unable to create tester class for the name: "<<name);
  return tester;
}

/// @brief structure to encapsulate test and its timing results
struct TestAndTimingResults {
   /// @brief shared pointer to the tester object (defines tests and results)
   boost::shared_ptr<ITester> itsTester;

   /// @brief initialisation time in seconds
   double itsInitTime;

   /// @brief run time in seconds
   double itsRunTime;

   /// @brief name of the test
   std::string itsName;

   /// @brief constructor setting up the test only (results to be filled later)
   /// @param[in] name test name
   /// @param[in] tester shared pointer to the tester object
   TestAndTimingResults(const std::string &name, const boost::shared_ptr<ITester> &tester) : itsTester(tester), itsName(name), itsInitTime(0.), itsRunTime(0.) {}
};

std::vector<TestAndTimingResults> doTests(const std::vector<std::string> &testNames, casacore::uInt sizeX, casacore::uInt sizeY)
{
   std::vector<TestAndTimingResults> testers;
   testers.reserve(testNames.size());
   // creation
   for (auto name : testNames) {
        testers.push_back(TestAndTimingResults(name, testFactory(name)));
   }
   std::cout<<"."<<std::flush;
   // initialisations
   for (std::vector<TestAndTimingResults>::iterator it = testers.begin(); it != testers.end(); ++it) {
        casacore::Timer timer;
        ASKAPDEBUGASSERT(it->itsTester);
        it->itsTester->init(sizeX,sizeY);
        it->itsInitTime = timer.real();
        std::cout<<"."<<std::flush;
   }
   // doing actual transforms
   for (std::vector<TestAndTimingResults>::iterator it = testers.begin(); it != testers.end(); ++it) {
        casacore::Timer timer;
        ASKAPDEBUGASSERT(it->itsTester);
        it->itsTester->run();
        it->itsRunTime = timer.real();
        std::cout<<"."<<std::flush;
   }
   return testers; 
}

/// @brief print the timing results
/// @param[in] results a vector with timing results
void reportTiming(const std::vector<TestAndTimingResults> &results)
{
   std::cout<<std::endl<<"Timing stats"<<std::endl;
   std::cout<<"------------"<<std::endl;
   // first print timings for initialisation and transforms
   std::cout<<"Name             Init time, s Transform time, s"<<std::endl;
   for (TestAndTimingResults result : results) {
        std::cout<<std::setw(16)<<result.itsName<<" "<<std::setw(12)<<result.itsInitTime<<" "<<std::setw(17)<<result.itsRunTime<<std::endl;
   }
}

/// @brief compare resulting grids between different tests
/// @param[in] results a vector with testers
/// @return a matrix filled with largest by absolute value difference between resulting grids for
/// the tester i and j. Indices match the supplied vector
casacore::Matrix<double> compareGrids(const std::vector<TestAndTimingResults> &results)
{
   casacore::Matrix<double> stats(results.size(), results.size(), 0.);
   for (casacore::uInt i = 0; i < stats.nrow(); ++i) {
        for (casacore::uInt j = 0; j < stats.nrow(); ++j) {
             // although things should be symmetric in an ideal case, type conversion may lead to small asymmetry.
             // only explicitly exclude the main diagonal
             if (i != j) {
                 ASKAPASSERT(results[i].itsTester);
                 ASKAPASSERT(results[j].itsTester);
                 stats(i,j) = results[i].itsTester->compare(*results[j].itsTester);
             }
        }
        std::cout<<"."<<std::flush;
   }
   return stats;
}

/// @brief report results of cross-comparing grids
/// @param[in] names a vector with test names
/// @param[in] matrix with relative differences
/// @param[in] addLabel suffix label for presentation purposes
void reportGridDiffs(const std::vector<std::string> &names, const casacore::Matrix<double> &stats, const std::string &addLabel = "")
{
   ASKAPASSERT(names.size() == stats.nrow());
   ASKAPASSERT(names.size() == stats.ncolumn());
   ASKAPCHECK(names.size() > 1, "More than one test is necessary for comparison");
   std::string label = "Max. difference between results";
   if (addLabel.size() > 0) {
       label += " " + addLabel;
   }
   std::cout<<std::endl<<label<<std::endl;
   std::fill(label.begin(), label.end(), '-');
   std::cout<<label<<std::endl;

   std::cout<<"Name / Name      ";
   for (size_t col = 0; col + 1 < names.size(); ++col) {
        std::cout<<std::setw(16)<<names[col]<<" ";
   }
   std::cout<<std::endl;
   for (casacore::uInt row = 1; row < stats.nrow(); ++row) {
        std::cout<<std::setw(16)<<names[row]<<" ";
        for (casacore::uInt col = 0; col < row; ++col) {
             std::cout<<std::setw(16)<<stats(row,col)<<" ";
        }
        std::cout<<std::endl;
   }
}

/// @brief a helper method to parse size string given by the command line
/// @param[in] sizeStr size string (e.g. "12880x12880")
/// @return a pair with X and Y sizes
std::pair<casacore::uInt, casacore::uInt> parseSizeStr(const std::string &sizeStr) {
   std::pair<casacore::uInt, casacore::uInt> result;
   const size_t pos = sizeStr.find("x");
   ASKAPCHECK((pos != std::string::npos) && (pos + 1 < sizeStr.size()), "Grid size should be specified as '640x480', you have "<<sizeStr);
   return std::pair<casacore::uInt, casacore::uInt>(utility::fromString<casacore::uInt>(sizeStr.substr(0,pos)),
          utility::fromString<casacore::uInt>(sizeStr.substr(pos + 1)));
}

int main(int argc, char **argv) 
{
   // We use logging in various areas in FFTW wrapper.
   // initialise the logger to be able to see the messages
   // (although the tests themselves would work without)
   // It may be worth to overhaul logging initialisation at some
   // point - currently I (MV) not sure the code below helps much,
   // but log messages appear. 
   const std::string defaultLogConfig("askap.log_cfg");
   const casacore::File logConfigFile(defaultLogConfig); 
   if (logConfigFile.exists()) {
       ASKAPLOG_INIT(defaultLogConfig.c_str());
   } else {
       const std::string specialLogConfig = std::string(argv[0]) + ".log_cfg";
       ASKAPLOG_INIT(specialLogConfig.c_str());
   }

   #ifdef _OPENMP
   std::cout<<"built with OpenMP support, maxThreads = "<<LOFAR::OpenMP::maxThreads()<<std::endl;
   #else 
   std::cout<<"built without OpenMP support"<<std::endl;
   #endif

   #ifdef HAVE_FFTW_OPENMP
   std::cout<<"built with FFTW OpenMP libraries"<<std::endl;
   #else
   std::cout<<"built without FFTW OpenMP libraries"<<std::endl;
   #endif

   std::string sizeStr = "12880x12880";
   if (argc > 3) {
      std::cerr<<"Usage: "<<argv[0]<<" ["<<sizeStr<<" [test_name]]"<<std::endl;
      return 1;
   }

   if (argc > 1) {
      sizeStr = std::string(argv[1]);
   }
   const std::pair<casacore::uInt, casacore::uInt> sizes = parseSizeStr(sizeStr);

   // default list of tests
   std::vector<std::string> testNames = {"new_wrapper_flt", "new_wrapper_dbl", "old_wrapper_flt", "old_wrapper_dbl","latticefft_flt", "latticefft_dbl"};
   if (argc > 2) {
       testNames = std::vector<std::string>(1u, argv[2]);
   }
   std::cout<<"Testing with a "<<sizes.first<<" x "<<sizes.second<<" grid"<<std::endl;
   ASKAPCHECK(sizes.first > 0 && sizes.second > 0, "Expected 2D grid with non-zero length on both axes");

   const std::vector<TestAndTimingResults> results = doTests(testNames, sizes.first, sizes.second);
   // this is safe to do even if there is just one grid, but the result would be meaningless in this case
   const casacore::Matrix<double> gridDiffs = compareGrids(results);
   std::cout<<std::endl;

   reportTiming(results);
   if (testNames.size() > 1) {
       reportGridDiffs(testNames, gridDiffs);
   }
   return 0;
}
